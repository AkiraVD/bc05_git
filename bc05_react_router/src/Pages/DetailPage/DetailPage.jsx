import { Progress } from "antd";
import axios from "axios";
import React, { Component } from "react";
import Header from "../../Components/Header";
import { createConfigHeader } from "../utils/utils";

export default class DetailPage extends Component {
  state = {
    data: {},
  };
  componentDidMount() {
    // console.log(this.props);
    let { id } = this.props.match.params;
    console.log("id: ", id);

    axios({
      url: `https://movienew.cybersoft.edu.vn/api/QuanLyPhim/LayThongTinPhim?MaPhim=${id}`,
      method: "GET",
      headers: createConfigHeader(),
    })
      .then((result) => {
        console.log(result.data.content);
        this.setState({
          data: result.data.content,
        });
      })
      .catch((err) => {
        console.log("err: ", err);
      });
  }
  render() {
    let { tenPhim, trailer, hinhAnh, danhGia } = this.state.data;
    return (
      <div className="container">
        <div>
          <h2>{tenPhim}</h2>
          <Progress
            type="circle"
            percent={danhGia * 10}
            format={(number) => {
              return `${number / 10} điểm`;
            }}
          />
          <br />
          {/* <img src={hinhAnh} alt="" /> */}
          <div style={{ position: "relative", width: "100%", height: "600px" }}>
            <iframe
              style={{
                width: "100%",
                height: "100%",
                position: "absolute",
                top: "0",
                left: "0",
                bottom: "0",
                right: "0",
                pointerEvents: "none",
              }}
              class="responsive-iframe"
              src={trailer + "?autoplay=1&mute=1"}
              frameBorder="0"
            ></iframe>
          </div>
        </div>
      </div>
    );
  }
}
