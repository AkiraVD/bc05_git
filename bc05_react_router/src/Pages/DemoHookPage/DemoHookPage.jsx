import React, { useEffect, useState } from "react";
import Banner from "./Banner";

export default function DemoHookPage() {
  useEffect(() => {
    console.log("DID MOUNT");
  }, []);
  // dismount, didupdate, willunmount
  let [like, setLike] = useState(1);
  let handlePlusLike = () => {
    setLike(like + 1);
  };

  let [share, setShare] = useState(1);
  let handleShare = () => {
    setShare(share + 1);
  };
  console.log("RENDER");
  return (
    <div>
      <h2>DemoHookPage</h2>
      <p className="display-4">
        LIKE:
        <span>{like}</span>
      </p>
      <button className="btn btn-success" onClick={handlePlusLike}>
        Plus like
      </button>
      <p className="display-4">
        SHARE:
        <span>{share}</span>
      </p>
      <button className="btn btn-secondary" onClick={handleShare}>
        Plus share
      </button>
      {like < 10 && <Banner title="Hello Cyber" handleClick={handlePlusLike} />}
    </div>
  );
}
