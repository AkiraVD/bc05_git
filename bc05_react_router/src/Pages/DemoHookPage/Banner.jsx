import React, { memo, useEffect } from "react";

function Banner({ title, handleClick }) {
  useEffect(() => {
    return () => {
      console.log("BANNER UNMOUNTED");
    };
  }, []);
  console.log("BANNER RENDERED");
  return (
    <div className="p-5 bg-dark text-white">
      Banner
      <h2>{title}</h2>
      <button className="btn btn-light" onClick={handleClick}>
        plus like
      </button>
    </div>
  );
}

export default memo(Banner);
