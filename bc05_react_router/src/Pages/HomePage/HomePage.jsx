import axios from "axios";
import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import Header from "../../Components/Header";
import { createConfigHeader, TOKEN_CYBERSOFT } from "../utils/utils";
import MovieItem from "./MovieItem";

export default class HomePage extends Component {
  state = {
    movieArr: [],
  };
  componentDidMount() {
    axios({
      url: "https://movienew.cybersoft.edu.vn/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP03",
      method: "GET",
      headers: createConfigHeader(),
    })
      .then((result) => {
        this.setState({
          movieArr: result.data.content,
        });
      })
      .catch((err) => {
        console.log("err: ", err);
      });
  }
  renderMovieList = () => {
    return this.state.movieArr.map((item, key) => {
      return (
        <div className="col-sm-3" key={key}>
          <MovieItem movie={item} />
        </div>
      );
    });
  };
  render() {
    return (
      <div className="container">
        <div className="row">{this.renderMovieList()}</div>
      </div>
    );
  }
}
