let renderRadioButton = (data) => {
  return `<div class="form-check">
    <label class="form-check-label">
    <input type="radio" class="form-check-input" name="singleChoice" id="" value="${data.exact}">
    ${data.content}
  </label>
</div>`;
};

let renderSingleChoice = (question) => {
  let contentAnswer = "";
  question.answers.forEach((item) => {
    contentAnswer += renderRadioButton(item);
  });
  return `
  <p>${question.content}</p>
  ${contentAnswer}
  `;
};

export const renderFillToInput = (question) => {
  return `
  <div class="form-group">
  <label for="">${question.content}</label>
  <input type="text"
  data-no-answer="${question.answers[0].content}"
    class="form-control" name="" id="fillInput" placeholder="">
</div>
  `;
};

export const checkSingleChoice = () => {
  let inputElement = document.querySelector(
    'input[name="singleChoice"]:checked'
  );
  if (inputElement == null || inputElement.value == "false") return false;
  if (inputElement.value == "true") return true;
};

export const checkFillInput = () => {
  let inputElement = document.getElementById("fillInput");

  if (inputElement.value == inputElement.dataset.noAnswer) {
    return true;
  } else return false;
};

export const renderQuestion = (question) => {
  if (question.questionType == 1) {
    //render single choice
    document.getElementById("contentQuiz").innerHTML =
      renderSingleChoice(question);
  } else {
    //render fill input
    document.getElementById("contentQuiz").innerHTML =
      renderFillToInput(question);
  }
};

export const showResult = (list) => {
  document.getElementById("endQuiz").classList.remove("d-none");
  document.getElementById("startQuiz").style.display = "none";

  // Đếm câu trả lời đúng
  let correct = 0;
  list.forEach((item) => {
    if (item.isCorrect) {
      correct++;
    }
    // item.isCorrect && correct++; Cách viết khác
  });

  document.getElementById("correct").innerHTML = correct;
  document.getElementById("incorrect").innerHTML = list.length - correct;
  document.getElementById("score").innerHTML = (correct * 100) / 8;
};
