import { dataQuestion as dataRaw } from "../data/questions.js";
import {
  checkFillInput,
  checkSingleChoice,
  renderQuestion,
  showResult,
} from "./controller.js";

let currentQuestionIndex = 0;

let dataQuestion = dataRaw.map((item) => {
  return { ...item, isCorrect: null };
});

console.log("dataQuestion: ", dataQuestion);
// render lần đầu
document.getElementById("currentStep").innerHTML = `${
  currentQuestionIndex + 1
}/${dataQuestion.length} `;

renderQuestion(dataQuestion[currentQuestionIndex]);

function nextQuestion() {
  // kiểm tra đáp án user chọn
  console.log("checkSingleChoice: ", checkSingleChoice());
  if (dataQuestion[currentQuestionIndex].questionType == 1) {
    dataQuestion[currentQuestionIndex].isCorrect = checkSingleChoice();
    console.table(dataQuestion);
  } else {
    checkFillInput();
    dataQuestion[currentQuestionIndex].isCorrect = checkFillInput();
    console.table(dataQuestion);
  }

  currentQuestionIndex++;
  if (currentQuestionIndex == dataQuestion.length) {
    showResult(dataQuestion);
    return;
  }

  document.getElementById("currentStep").innerHTML = `${
    currentQuestionIndex + 1
  }/${dataQuestion.length} `;

  //render nội dung câu hỏi
  renderQuestion(dataQuestion[currentQuestionIndex]);
}

window.nextQuestion = nextQuestion;
