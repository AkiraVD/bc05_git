export function layThongTinTuForm() {
  let id = document.getElementById("foodID").value;
  let name = document.getElementById("tenMon").value;
  let type = document.getElementById("loai").value;
  let price = document.getElementById("giaMon").value;
  let disc = document.getElementById("khuyenMai").value;
  let stat = document.getElementById("tinhTrang").value;
  let img = document.getElementById("hinhMon").value;
  let desc = document.getElementById("moTa").value;

  return { id, name, type, price, disc, stat, img, desc };
}

export default function showThongTinLenForm(data) {
  let { id, name, type, price, disc, stat, img, desc } = data;
  document.getElementById("imgMonAn").src = img;
  document.getElementById("spMa").innerText = id;
  document.getElementById("spTenMon").innerText = name;
  document.getElementById("spLoaiMon").innerText = type;
  document.getElementById("spGia").innerText = price;
  document.getElementById("spKM").innerText = disc;
  document.getElementById("spTT").innerText = stat;
  document.getElementById("pMoTa").innerText = desc;
  document.getElementById("spGiaKM").innerText = data.calculateDiscount();
}

//1 file chỉ được export default 1 lần duy nhất
