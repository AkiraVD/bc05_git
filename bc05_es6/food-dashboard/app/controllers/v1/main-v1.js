import { layThongTinTuForm } from "./controller-v1.js";
import hienThiThongTin from "./controller-v1.js";
import { MonAn } from "../../models/v1/monAnModel-v1.js";

document.getElementById("btnThemMon").addEventListener("click", function () {
  let data = layThongTinTuForm();

  // let monAn = new MonAn(
  //   data.id,
  //   data.name,
  //   data.type,
  //   data.price,
  //   data.disc,
  //   data.stat,
  //   data.img,
  //   data.desc
  // );

  let { id, name, type, price, disc, stat, img, desc } = data;

  let monAn = new MonAn(id, name, type, price, disc, stat, img, desc);
  hienThiThongTin(monAn);
});
