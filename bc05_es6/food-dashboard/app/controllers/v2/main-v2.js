import { deleteFoodbyID, getAllFood } from "../../service/service.js";
import { renderFoodList } from "./controller-v2.js";

let fetchAllFoodService = () => {
  getAllFood()
    .then((result) => {
      console.log("result: ", result);
      renderFoodList(result.data);
    })
    .catch((err) => {
      console.log("err: ", err);
    });
};
fetchAllFoodService();

function xoaMonAn(id) {
  deleteFoodbyID(id)
    .then((res) => {
      Swal.fire("Xóa món ăn thành công");
      fetchAllFoodService();
    })
    .catch((err) => {
      Swal.fire({
        icon: "error",
        title: "Ui cha",
        text: "Xóa không thành công rồi",
      });
      console.log("err: ", err);
    });
}

window.xoaMonAn = xoaMonAn;
