export const renderFoodList = (list) => {
  let contentHTML = "";

  list.forEach((item) => {
    let { id, name, type, price, disc, stat } = item;
    contentHTML += `
        <tr>
            <td>${id}</td>
            <td>${name}</td>
            <td>${type ? "Mặn" : "Chay"}</td>
            <td>${price}</td>
            <td>${disc}%</td>
            <td>0</td>
            <td>${stat ? "Còn" : "Hết"}</td>
            <td>
            <button onclick="xoaMonAn(${id})" class="btn btn-danger"><i class="fa fa-times"></i></button>
            <button onclick="" class="btn btn-secondary"><i class="fa fa-edit"></i></button>
            </td>
        </tr>
        `;
  });

  document.getElementById("tbodyFood").innerHTML = contentHTML;
};

// type: true => mặn
// status: true => còn
