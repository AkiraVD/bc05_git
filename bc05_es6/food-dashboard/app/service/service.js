let BASE_URL = "https://635f4b193e8f65f283b01221.mockapi.io";

export let getAllFood = () => {
  return axios({
    url: `${BASE_URL}/food`,
    method: "GET",
  });
};

export let deleteFoodbyID = (id) => {
  return axios({
    url: `${BASE_URL}/food/${id}`,
    method: "DELETE",
  });
};
