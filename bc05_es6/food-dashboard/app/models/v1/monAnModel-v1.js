export class MonAn {
  //   constructor(name, price) {
  //     this.name = name;
  //     this.price = price;
  //   }
  //   khuyenMai = function () {
  //     console.log("10%", this.name);
  //   };
  constructor(id, name, type, price, disc, stat, img, desc) {
    this.id = id;
    this.name = name;
    this.type = type;
    this.price = price;
    this.disc = disc;
    this.stat = stat;
    this.img = img;
    this.desc = desc;
  }
  calculateDiscount = () => {
    return this.price * (1 - this.disc / 100);
  };
}
