console.log(`-------------------------------------`);
// dùng cho Object hoặc Array

//object
let cat1 = {
  score: 10,
};

let cat2 = { ...cat1, score: 2, name: "Tom" };
console.log("cat1: ", cat1);
console.log("cat2: ", cat2);

cat2.score = 2;
console.log("cat2.score: ", cat2.score);
console.log("cat1.score: ", cat1.score);

console.log();

//array

let cats = ["tom", "Tun"];

let newCats = [...cats, "Tit", "meo"];
console.log("cats: ", cats);
console.log("newCats: ", newCats);
