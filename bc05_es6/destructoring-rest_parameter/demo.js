let username = "Alice";
console.log({ username });

let dogs = {
  name: "Bull",
  age: 2,
};

// Object bắt buộc tên phải giống
let { age, name } = dogs;
console.log("name: ", name);
console.log("age: ", age);

// Array chỉ cần đúng thứ tự
let colors = ["red", "black"];
let [c1] = colors;
console.log("c1: ", c1);

// rest parameter
// dùng khi tham số truyền vào không cố định
function tinhTong(...nums) {
  console.log("nums: ", nums);

  let sums = 0;
  nums.forEach((item) => {
    sums += item;
  });
  console.log("sums: ", sums);
}
tinhTong(2, 4, 5, 6, 8, 9, 2);
