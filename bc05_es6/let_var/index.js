// var có cơ chế hosting

// scope : Phạm vi hoạt động

// var: function scope

function checkLogin(isLogin) {
  if (isLogin) {
    var loginTime = "Today";
    console.log("loginTime: ", loginTime);
  }
}

// let: block scope

checkLogin(true);
