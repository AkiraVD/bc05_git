console.log("Hellow World");

var username = "Phuong";
// "Phuong" type string

console.log("username");
console.log(username);

var age = 2;
// 2 type string

var isMarried = false;
var isLogin = true;
// true, false type boolean

isMarried = true;
// Upadate value

// camel case:

// snake case:
// var is_Married = null;
var is_Married = undefined;

var username1 = "phuong";
var username2 = "miller";

// Operator

var num1 = 2;
var num2 = 3;

var num3 = num2 + num1;

var num4 = 4;
// num4 = num4 + 2;
num4 += 2;

console.log("num3: ", num3);
console.log("num4: ", num4);

var num5 = 2 / 5;
console.log("num5: ", num5);

var num6 = 2 + 10 / 2;
console.log("num6: ", num6);

var num7 = 22 % 5;
console.log("num7: ", num7);
