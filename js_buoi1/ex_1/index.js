/**
 * Input : độ dài 2 cạnh góc vuông 3 , 4
 *
 * Step :
 * step 1: tạo 2 biến chứa 2 cạnh góc vuông
 * step 2: tạo biến chứa kết quả cần tìm
 * step 3: áp dụng công thức Pytago
 *
 * Output : 5
 */

var edge1 = 3;
var edge2 = 4;
var edge3 = null;

edge3 = Math.sqrt(edge1 * edge1 + edge2 * edge2);

console.log("result :", edge3);
/**
 * Bài 2:
 *
 * 123 => 6
 * 246 => 12
 *
 * Input: Số có 3 chữ số
 *
 * Step:
 * 1/ tạo biến chứa số có 3 chữ số
 * 2/ tạo 3 biến chứa 3 chữ số hàng trăm, chục, đơn vị
 *
 * Output: tổng của trăm, chục, đơn vị
 *
 */
var number = 625;
var donVi = number % 10;
var chuc = Math.floor((number % 100) / 10);
var tram = Math.floor(number / 100);

var tong = donVi + chuc + tram;
console.log("tong: ", tong);
