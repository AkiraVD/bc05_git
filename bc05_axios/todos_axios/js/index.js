const BASE_URL = "https://635f4b193e8f65f283b01221.mockapi.io";

var idEdited = null;
//Render all todos service
function fetchAllTodo() {
  turnOnLoading();
  axios({
    url: `${BASE_URL}/todos`,
    method: "GET",
  })
    .then(function (res) {
      //   console.log("res: ", res.data);
      turnOffLoading();
      renderTodoList(res.data);
    })
    .catch(function (err) {
      turnOffLoading();
      console.log("err: ", err);
    });
}

//Chạy lần đầu
fetchAllTodo();

//Remove todos service
function removeTodo(idTodo) {
  turnOnLoading();
  //   console.log("id: ", idTodo);
  axios({
    url: `${BASE_URL}/todos/${idTodo}`,
    method: "DELETE",
  })
    .then(function (res) {
      fetchAllTodo();
    })
    .catch(function (err) {
      turnOffLoading();
      console.log("err: ", err);
    });
}

function addTodo() {
  var data = getInfoFromForm();

  var newTodo = {
    name: data.name,
    desc: data.desc,
    isComplete: false,
  };
  console.log("newTodo: ", newTodo);
  turnOnLoading();
  axios({
    url: `${BASE_URL}/todos`,
    method: "POST",
    data: newTodo,
  })
    .then(function (res) {
      console.log("res: ", res.data);
      fetchAllTodo();
    })
    .catch(function (err) {
      turnOffLoading();
      console.log("err: ", err);
    });
}

function editTodo(idTodo) {
  turnOnLoading();
  axios({
    url: `${BASE_URL}/todos/${idTodo}`,
    method: "GET",
  })
    .then(function (res) {
      turnOffLoading();
      //Show thông tin lên form
      document.getElementById("name").value = res.data.name;
      document.getElementById("desc").value = res.data.desc;
      idEdited = res.data.id;
    })
    .catch(function (err) {
      console.log("err: ", err);
    });
}

function updateTodo() {
  turnOnLoading();

  let data = getInfoFromForm();
  axios({
    url: `${BASE_URL}/todos/${idEdited}`,
    method: "PUT",
    data: data,
  })
    .then(function (res) {
      turnOffLoading();
      fetchAllTodo();
    })
    .catch(function (err) {
      console.log("err: ", err);
    });
}
