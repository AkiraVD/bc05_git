function renderTodoList(todos) {
  var contentHTML = "";

  todos.forEach(function (item) {
    var content = `<tr>
          <td>${item.id}</td>
          <td>${item.name}</td>
          <td>${item.desc}</td>
          <td><input type="checkbox" ${item.isComplete ? "checked" : ""}></td>
          <td><a href="#form"><button class="btn btn-secondary mr-2" onclick="editTodo(${
            item.id
          })">Sửa</button></a><button class="btn btn-danger" onclick="removeTodo(${
      item.id
    })">Delete</button></td>
      </tr>`;
    contentHTML += content;
  });
  document.getElementById("tbody-todos").innerHTML = contentHTML;
}

function turnOnLoading() {
  document.getElementById("loading").style.display = "flex";
}

function turnOffLoading() {
  document.getElementById("loading").style.display = "none";
}

function getInfoFromForm() {
  var name = document.getElementById("name").value;
  var desc = document.getElementById("desc").value;
  return {
    name: name,
    desc: desc,
  };
}
