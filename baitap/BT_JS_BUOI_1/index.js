/**
 * Bài 1: Tính lương nhân viên
 *
 * Input: Số ngày làm
 *
 * Step:
 * 1/ Tạo biến Số ngày làm, hằng số Lương 1 ngày, biến số Lương tháng
 * 2/ Lương tháng = số ngày làm * lương 1 ngày
 *
 * Output: Lương tháng
 */

var days = 24;
const DAILY_SALARY = 100000;
var monthSalary;

monthSalary = days * DAILY_SALARY;
// 24 * 100000 = 2400000
console.log("Bài 1:");
console.log("Ngày làm: ", days);
console.log("Lương tháng: ", monthSalary, "đ");

/**
 * Bài 2: Tính giá trị trung bình
 *
 * Input: 5 số thực
 *
 * Step:
 * 1/ tạo 5 biến cho 5 số thực, biến giá trị trung bình
 * 2/ Giá trị trung bình = (tổng 5 số thực)/5
 *
 * Output: Giá trị trung bình
 */

var number1 = 2;
var number2 = 3;
var number3 = 5;
var number4 = 6;
var number5 = 9;
var averageValue;

averageValue = (number1 + number2 + number3 + number4 + number5) / 5;
// 2 + 3 + 5 + 6 + 9 = 25
// 25 / 5 = 5
console.log("Bài 2:");
console.log(
  "5 số thực",
  number1,
  " ",
  number2,
  " ",
  number3,
  " ",
  number4,
  " ",
  number5
);
console.log("Giá trị trung bình: ", averageValue);

/** Bài 3: Quy đổi tiền
 *
 * Input: Số tiền = USD
 *
 * Step:
 * 1/ tạo biến số tiền = USD, số tiền = VND, tý giá.
 * 2/ Số tiền VND = số tiền USD * tỷ giá
 *
 * Output: Số tiền = VND
 */

var usdMoney = 2;
var usdPerVnd = 23500;
var vndMoney;

vndMoney = usdMoney * usdPerVnd;

console.log("Bài 3:");
console.log("Số tiền USD: ", "$", usdMoney);
console.log("Tỷ giá: ", usdPerVnd);
console.log("Quy đổi sang VND: ", vndMoney, "đ");

/** Bài 4: Diện tích, chu vi hình chữ nhật
 *
 * Input: Chiều dài, chiều rộng
 *
 * Step:
 * 1/ tạo biến Chiều dài, chiều rộng, chu vi, diện tích
 * 2/ Chu vi = (chiều dài + chiều rộng) * 2
 * 3/ diện tích = chiều dài * chiều rộng
 *
 * Output: Chu vi, diện tích
 */

var recH = 3;
var recW = 4;
var perimeter;
var area;

perimeter = (recH + recW) * 2;
area = recH * recW;

console.log("Bài 4:");
console.log("Chiều dài: ", recH);
console.log("Chiều rộng: ", recW);
console.log("Chu vi : ", perimeter);
console.log("Diện tích: ", area);

/** Bài 5: Tổng 2 ký số
 *
 * Input: 1 số có 2 chữ số
 *
 * Step:
 * 1/ tạo biến chứa số có 2 chữ số, số hàng đơn vị, số hàng chục, tổng 2 ký số
 * 2/ Số hàng đơn vị = số lẻ khi số đó chia cho 10
 * 3/ Số hàng chục = lấy số đó chia 10 rồi làm tròn xuống
 * 4/ tổng 2 ký số = số hàng chục + số hàng đơn vị
 * Output: Tổng 2 ký số
 */

var number = 83;
var tenth;
var unit;
var total;

unit = number % 10;
tenth = Math.floor(number / 10);
total = unit + tenth;

console.log("Bài 5:");
console.log("Số có 2 chữ số: ", number);
console.log("total: ", total);
