/*
Bài 1: Quản lý tuyển sinh
    Input: 
    -Điểm chuẩn hội động
    -Điểm 3 môn thí sinh
    -Khu vực
    -Đối tượng
    Output:
    -Tổng điểm
    -Đậu hoặc rớt
    Step:
    1/Function xác định điểm cộng khu vực
    2/Function xác định điểm cộng đối tượng
    3/Function tính điểm tổng thí sinh + điểm cộng thí sinh và so sánh với điểm chuẩn hội đồng
    4/xuất ra kết quả
*/
function diemKV(vung) {
  switch (vung) {
    case "A": {
      return 2;
    }
    case "B": {
      return 1;
    }
    case "C": {
      return 0.5;
    }
    case "D": {
      return 0;
    }
  }
}

function diemDT(doituong) {
  switch (doituong) {
    case "1": {
      return 2.5;
    }
    case "2": {
      return 1.5;
    }
    case "3": {
      return 1;
    }
    case "4": {
      return 0;
    }
  }
}

function checkDiem() {
  var vung = document.querySelector('input[name="selectVung"]:checked').value;
  var doituong = document.querySelector(
    'input[name="selectDoiTuong"]:checked'
  ).value;

  var diemChuan = document.getElementById("diemChuan").value * 1;
  var diemMon1 = document.getElementById("diem1").value * 1;
  var diemMon2 = document.getElementById("diem2").value * 1;
  var diemMon3 = document.getElementById("diem3").value * 1;

  var diemCongKV = diemKV(vung) * 1;
  var diemCongDT = diemDT(doituong) * 1;

  var tongDiem = diemMon1 + diemMon2 + diemMon3 + diemCongDT + diemCongKV;
  var dauRot;
  if (tongDiem < diemChuan) {
    dauRot = "Xin chia buồn, bạn đã thi rớt";
  } else {
    dauRot = "Xin chúc mừng, bạn đã thi đậu";
  }
  document.getElementById(
    "ketQuaThi"
  ).innerHTML = `Tổng điểm của bạn là: ${tongDiem}<br/> ${dauRot}`;
}

/*
Bài 2: Tính tiền điện
    Input: 
    -Tên
    -Số Kw tiêu thụ (dienTieuThu)
    Output:
    -Tổng tiền điện (tongTienDien)
    Step:
    1/Lấy giá trị Kw tiêu thụ và so sánh:
    Nếu dienTieuThu <= 50kW:
    - tongTienDien = dienTieuThu * 500;
    Nếu 50kW < dienTieuThu <= 100kW:
    - tongTienDien = 50 * 500 + (dienTieuThu - 50) * 650;
    Nếu 100kW < dienTieuThu <= 200kW:
    - tongTienDien = 50 * (500 + 650) + (dienTieuThu - 100) * 850;
    Nếu 200kW < dienTieuThu <= 350kW:
    - tongTienDien = 50 * (500 + 650) + 100 * 850 + (dienTieuThu - 200) * 1100;
    Nếu dienTieuThu > 350kW:
    - tongTienDien = 50 * (500 + 650) + 100 * 850 + 150 * 1100 + (dienTieuThu - 350) * 1100;

    2/Xuất ra tổng tiền điện
*/
function tinhTienDien() {
  tenKH = document.getElementById("tenKH").value;
  var dienTieuThu = document.getElementById("dienTieuThu").value * 1;
  var tongTienDien = 0;
  if (dienTieuThu <= 50) {
    tongTienDien = dienTieuThu * 500;
  } else if (dienTieuThu <= 100) {
    tongTienDien = 50 * 500 + (dienTieuThu - 50) * 650;
  } else if (dienTieuThu <= 200) {
    tongTienDien = 50 * (500 + 650) + (dienTieuThu - 100) * 850;
  } else if (dienTieuThu <= 350) {
    tongTienDien = 50 * (500 + 650) + 100 * 850 + (dienTieuThu - 200) * 1100;
  } else if (dienTieuThu > 350) {
    tongTienDien =
      50 * (500 + 650) + 100 * 850 + 150 * 1100 + (dienTieuThu - 350) * 1100;
  }

  document.getElementById(
    "tongTienDien"
  ).innerHTML = `Tên khách hàng: ${tenKH} <br/>
  Số tiền điện cần thanh toán: ${tongTienDien} VNĐ`;
}
