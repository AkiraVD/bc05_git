function phanTramThue(thuNhap) {
  var phanTramThueNop = 0;
  if (thuNhap <= 60) {
    phanTramThueNop = 0.05;
  } else if (thuNhap <= 120) {
    phanTramThueNop = 0.1;
  } else if (thuNhap <= 210) {
    phanTramThueNop = 0.15;
  } else if (thuNhap <= 384) {
    phanTramThueNop = 0.2;
  } else if (thuNhap <= 624) {
    phanTramThueNop = 0.25;
  } else if (thuNhap <= 960) {
    phanTramThueNop = 0.3;
  } else {
    phanTramThueNop = 0.35;
  }
  return phanTramThueNop;
}

function tinhThue() {
  var hoTen = document.getElementById("hoTen").value;
  var thuNhapNam = document.getElementById("thuNhapNam").value * 1;
  var soNgPhuThuoc = document.getElementById("soNgPhuThuoc").value * 1;

  var thuNhapChiuThue = thuNhapNam - 4 - soNgPhuThuoc * 1.6;

  var phanTramThueNop = phanTramThue(thuNhapChiuThue);

  var tienThue =
    Math.round(phanTramThueNop * thuNhapChiuThue * 100000000) / 100;
  console.log("thuNhapChiuThue: ", thuNhapChiuThue);

  document.getElementById("tienThue").innerHTML = `
  Khách hàng: ${hoTen} </br>
  Số tiền thuế: ${tienThue} đồng`;
}
