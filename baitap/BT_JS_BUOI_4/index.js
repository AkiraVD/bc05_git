/*
Bài 1
Input : 3 số nguyên
Output: 3 số theo thứ tự tăng dần
Step:
    1/ Nhập 3 số nguyên
    2/ Nếu số 1 > số 2 và số 1 < số 3 => Thứ tự 2 < 1 < 3
    3/ Nếu số 1 < số 2 và số 1 > số 3 => Thứ tự 3 < 1 < 2
    4/ Trường hợp số 1 > số 2 và số 1 > số 3:
    - Nếu số 2 > số 3 => thứ tự 3 < 2 < 1
    - Nếu số 3 > số 2 => thứ tự 2 < 3 < 1
    5/ Trường hợp số 1 < số 2 và số 1 < số 3:
    - Nếu số 2 > số 3 => thứ tự 1 < 3 < 2
    - Nếu số 3 > số 2 => thứ tự 1 < 2 < 3
*/

function checkOrder() {
  var small;
  var mid;
  var big;
  var numb1 = document.getElementById("numb1").value * 1;
  var numb2 = document.getElementById("numb2").value * 1;
  var numb3 = document.getElementById("numb3").value * 1;

  if (numb1 > numb2 && numb1 < numb3) {
    small = numb2;
    mid = numb1;
    big = numb3;
  } else if (numb1 < numb2 && numb1 > numb3) {
    small = numb3;
    mid = numb1;
    big = numb2;
  } else if (numb1 > numb2 && numb1 > numb3) {
    big = numb1;
    if (numb2 > numb3) {
      small = numb3;
      mid = numb2;
    } else {
      small = numb2;
      mid = numb3;
    }
  } else {
    small = numb1;
    if (numb2 > numb3) {
      mid = numb3;
      big = numb2;
    } else {
      mid = numb2;
      big = numb3;
    }
  }

  document.getElementById("numbOrder").innerHTML = `${small}, ${mid}, ${big}`;
}

/*
Bài 2
Input : Chọn 1 trong các thành viên trong gia đình
Output: Lời chào dựa theo thành viên đã chọn
Step:
    1/ tạo biến chứa tên thành viên
    2/ tạo các lời chào khác nhau dựa theo từng thành viên
    3/ kiểm tra biến rồi xuất ra lời chào tương ứng
*/

function xinChao() {
  var thanhVien;
  var loiChao;

  thanhVien = document.getElementById("famName").value;

  if (thanhVien == 1) {
    loiChao = "Xin chào ba, chúc công việc của ba thuận lợi";
  } else if (thanhVien == 2) {
    loiChao = "Chào mẹ, chúc mẹ một ngày tốt lành";
  } else if (thanhVien == 3) {
    loiChao = "Chào anh hai, chúc anh hai học tập tốt";
  } else {
    loiChao = "Chào em, chúc em học bài tốt";
  }

  document.getElementById("xinChao").innerHTML = `
  <p  style="text-align: center"
      class="text-success display-4"
  >
  ${loiChao}
  </p>
  `;
  console.log("yes");
  console.log(thanhVien);
}

/*
Bài 3
Input : viết 3 số nguyên
Output: Có bao nhiêu số lẻ, bao nhiêu số chắn
Step:
    1/ tạo các giá trị chứa 3 số nguyên
    2/ tạo 2 biến chứa số lẻ và chứa số chẵn
    3/ kiểm tra từng số theo thứ tự
    nếu số đó chia hết cho 2 => tổng số chẵn += 1
    nếu số đó không chia hết cho 2 => tổng số lẻ += 1
    nếu số đó == 0 => tổng số chắn += 1
    4/ xuất ra tổng các số chẵn và số lẻ
*/
// dùng lại 3 var ở bài trên
function demChanLe() {
  var soChan = 0;
  var soLe = 0;
  var chanLe1 = document.getElementById("chanLe1").value;
  var chanLe2 = document.getElementById("chanLe2").value;
  var chanLe3 = document.getElementById("chanLe3").value;

  if (chanLe1 % 2 == 0) {
    soChan += 1;
  } else if (chanLe1 % 2 == 1) {
    soLe += 1;
  }

  if (chanLe2 % 2 == 0) {
    soChan += 1;
  } else if (chanLe2 % 2 == 1) {
    soLe += 1;
  }

  if (chanLe3 % 2 == 0) {
    soChan += 1;
  } else if (chanLe3 % 2 == 1) {
    soLe += 1;
  }

  document.getElementById(
    "tongChanLe"
  ).innerHTML = `<p>Có ${soChan} số chẵn và ${soLe} số lẻ</p>`;
  soChan = 0;
  soLe = 0;
}

/* 
Bài 3
Input : Nhập 3 cạnh của tam giác
Output: Tam giác đó là tam giác cân, đều hay vuông
Step:
  1/ tạo 3 giá trị chứa 3 cạnh tam giác
  2/ kiểm tra 3 cạnh của tam giác
    Nếu 1 cạnh lớn hơn hoặc bằng tổng 2 cạnh còn lại => không phải tam giác
    Nếu cả 3 cạnh bằng nhau => tam giác đều
    Nếu cả 2 cạnh bằng nhau => tam giác cân
    Nếu bình phương 1 cạnh bằng tổng bình phương 2 cạnh còn lại => tam giác vuông  
*/

function checkTri() {
  var triangleType;
  var edgeA = document.getElementById("edgeA").value * 1;
  var edgeB = document.getElementById("edgeB").value * 1;
  var edgeC = document.getElementById("edgeC").value * 1;
  var edgeA2 = edgeA * edgeA;
  var edgeB2 = edgeB * edgeB;
  var edgeC2 = edgeC * edgeC;
  var sumB2C2 = edgeC * edgeC + edgeB * edgeB;
  var sumC2A2 = edgeA * edgeA + edgeC * edgeC;
  var sumA2B2 = edgeA * edgeA + edgeB * edgeB;

  if (
    edgeA >= edgeB + edgeC ||
    edgeB >= edgeA + edgeC ||
    edgeC >= edgeA + edgeB
  ) {
    triangleType = "Đây không phải là tam giác";
  } else if (edgeA == edgeB && edgeA == edgeC) {
    triangleType = "Đây là tam giác đều";
  } else if (edgeA == edgeB || edgeA == edgeC || edgeB == edgeC) {
    triangleType = "Đây là tam giác cân";
  } else if (edgeA2 == sumB2C2 || edgeB2 == sumC2A2 || edgeC2 == sumA2B2) {
    triangleType = "Đây là tam giác vuông";
  } else {
    triangleType = "Đây là tam giác thường";
  }

  document.getElementById("triangleType").innerHTML = triangleType;
}
