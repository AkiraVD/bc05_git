/*
Bài 1 tính toán ngày hôm trước và hôm sau:
Input:
-ngày, tháng, năm bất kì (Mặc định giá trị hợp lệ)
Output:
-Ngày hôm trước
-Ngày hôm sau
Step:
1/ Tạo biến chứa, ngày, tháng, năm

(optional) Funtcion Kiểm tra ngày có hợp lệ không:
    -Trường hợp ngày <= 0, hoặc tháng <= 0 hoặc tháng >= 13 hoặc năm <= 0 => không hợp lệ
    -Trường hợp tháng 1, 3, 5, 7, 8, 10, 12 => Nếu ngày > 32 => không hợp lệ 
    -Trường hợp tháng 4, 6, 9, 11 => Nếu ngày > 31 => không hợp lệ
    -Trường hợp tháng 2:
        +Nếu là năm nhuận = Nếu ngày > 29 => không hợp lệ
        +Nếu là năm không nhuận = Nếu ngày > 28 => không hợp lệ

2/ tạo funtion tính toán ngày hôm trước:
    Trường hợp ngày khác 1:
    -Ngày hôm trước là giá trị ngày trừ 1
    Trường hợp ngày 1:
    -Nếu tháng = 2, 4, 6, 8, 9, 11 (những tháng mà tháng đằng trước có 31 ngày) => ngày = 31, giá trị tháng - 1
    -Nếu tháng = 5, 7, 10, 12 (những tháng mà tháng đằng trước có 30 ngày)=> ngày = 30, giá trị tháng - 1
    -Nếu là tháng = 1 => ngày = 31, tháng = 12, giá trị năm - 1  
    -Nếu là tháng 3:
        -Tháng = 2
        + Trường hợp năm chia hết cho 4 => ngày = 29
        + Trường hợp còn lại => ngày = 28

3/ tạo function tính toán ngày hôm sau:
    Chia làm các trường hợp:
    -Tháng 1, 3, 5, 7, 8, 10 (tháng có 31 ngày):
    +Nếu ngày = 31 => giá trị ngày = 1, tháng + 1
     +Nếu ngày < 31 => giá trị ngày + 1
    -Tháng 4, 6, 9, 11 (tháng có 30 ngày)
    +Nếu ngày = 30 => giá trị ngày = 1, tháng + 1
     +Nếu ngày < 30 => giá trị ngày + 1
     -Tháng 12 => 
     +Nếu ngày = 31 => Ngày = 1, tháng = 1, năm -1
     +Nếu ngày < 31 => Ngày + 1
    -tháng 2:
     +Năm nhuận:
     =Ngày < 29 => ngày + 1
     =Ngày = 29 => ngày = 1, tháng = 3
     +Năm còn lại:
     =Ngày < 28 => ngày + 1
     =Ngày = 28 => ngày = 1, tháng = 3
        */

var day = 0;
var month = 0;
var year = 0;
var leapYear = false;
var dateCorrect;

function getDate1() {
  day = document.getElementById("day").value * 1;
  month = document.getElementById("month").value * 1;
  year = document.getElementById("year").value * 1;
}

function checkLeapYear() {
  if (year % 4 == 0) {
    leapYear = true;
  } else {
    leapYear = false;
  }
}

function checkCorrect() {
  checkLeapYear();

  if (day <= 0 || month <= 0 || month >= 13 || year <= 0) {
    dateCorrect = false;
    return;
  }
  if (
    month == 1 ||
    month == 3 ||
    month == 5 ||
    month == 7 ||
    month == 8 ||
    month == 10 ||
    month == 12
  ) {
    if (day > 31) {
      dateCorrect = false;
    } else {
      dateCorrect = true;
    }
  } else if (month == 4 || month == 6 || month == 9 || month == 11) {
    if (day > 30) {
      dateCorrect = false;
    } else {
      dateCorrect = true;
    }
  } else if (month == 2) {
    switch (leapYear) {
      case true: {
        if (day > 29) {
          dateCorrect = false;
        } else {
          dateCorrect = true;
        }
        break;
      }
      case false: {
        if (day > 28) {
          dateCorrect = false;
        } else {
          dateCorrect = true;
        }
        break;
      }
    }
  }
}
function checkYesterday() {
  getDate1();
  checkCorrect();
  if (day != 1) {
    day--;
  } else {
    if (
      month == 2 ||
      month == 4 ||
      month == 6 ||
      month == 8 ||
      month == 9 ||
      month == 11
    ) {
      day = 31;
      month--;
    } else if (month == 5 || month == 7 || month == 10 || month == 12) {
      day = 30;
      month--;
    } else if (month == 1) {
      day = 31;
      month = 12;
      year--;
    } else if (month == 3) {
      month = 2;
      if (leapYear) {
        day = 29;
      } else {
        day = 28;
      }
    }
  }

  if (dateCorrect == false) {
    document.getElementById(
      "showDate"
    ).innerHTML = `Ngày bạn nhập không hợp lệ`;
    return;
  } else {
    document.getElementById(
      "showDate"
    ).innerHTML = `Ngày hôm trước là: Ngày ${day} tháng ${month} năm ${year}`;
  }
}

function checkTomorrow() {
  getDate1();
  checkCorrect();
  switch (month) {
    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10: {
      if (day == 31) {
        day = 1;
        month++;
      } else {
        day++;
      }
      break;
    }
    case 4:
    case 6:
    case 9:
    case 11: {
      if (day == 30) {
        day = 1;
        month++;
      } else {
        day++;
      }
      break;
    }
    case 12: {
      if (day == 31) {
        day = 1;
        month = 1;
        year++;
      } else day++;
      break;
    }
    case 2: {
      if (leapYear) {
        if (day == 29) {
          day = 1;
          month = 3;
        } else day++;
      } else {
        if (day == 28) {
          day = 1;
          month = 3;
        } else day++;
      }
      break;
    }
  }
  if (dateCorrect == false) {
    document.getElementById(
      "showDate"
    ).innerHTML = `Ngày bạn nhập không hợp lệ`;
    return;
  } else {
    document.getElementById(
      "showDate"
    ).innerHTML = `Ngày hôm sau là: Ngày ${day} tháng ${month} năm ${year}`;
  }
}

/*
Bài 2: Tìm số ngày trong tháng
Input: Tháng, năm
Output: Số ngày trong tháng
Step: 
    1/Lấy giá trị ngày tháng, năm
    2/Dùng lại function check ngày và check năm nhuận của bài 1
    3/Trường hợp tháng 1, 3, 5, 7, 8, 10, 12 => có 31 ngày
    Trường hợp tháng 4, 6, 9, 11 => có 30 ngày
    Trường hợp tháng 2:
    -Năm nhuận => 29 ngày
    -năm còn lại => 30 ngày
*/
function getDate2() {
  month = document.getElementById("month2").value * 1;
  year = document.getElementById("year2").value * 1;
  day = 1;
}
function checkDayInMonth() {
  getDate2();
  checkCorrect();
  switch (month) {
    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
    case 12: {
      day = 31;
      break;
    }
    case 4:
    case 6:
    case 9:
    case 11: {
      day = 30;
      break;
    }
    case 2: {
      if (leapYear) {
        day = 29;
        break;
      } else {
        day = 28;
        break;
      }
    }
  }

  if (dateCorrect == false) {
    document.getElementById(
      "showDays"
    ).innerHTML = `Ngày bạn nhập không hợp lệ`;
    return;
  } else {
    document.getElementById(
      "showDays"
    ).innerHTML = `Tháng ${month} năm ${year} có ${day} ngày`;
  }
}

/*
Bài 3: Cách đọc số có 3 chữ số
Input: Số có 3 chữ số
Output: cách đọc
Step:
1/ Tạo biến số có 3 chữ số, số hàng chục, số hàng trăm, số hàng đơn vị
Kiểm tra Số < 1000:
2/ Tính số hàng trăm, chục, đơn vị, cách đọc từ 1 -> 10:

3/ Trường hợp hàng trăm = 0:
    -Nếu hàng chục = 0:
        =>Cách đọc = Số đơn vị
    -Nếu hàng chục = 1:
        +Nếu đơn vị = 5
            =>Cách đọc = "Mười Lăm"
        +Còn lại:
            =>Cách đọc = "Mười" + số đơn vị
    -Nếu hàng chục > 0:
        +Nếu đơn vị = 5
            =>Cách đọc = số hàng chục + "mươi" + "lăm"
            +Nếu đơn vị = 1
            =>Cách đọc = số hàng chục + "mươi" + "mốt"
        +Còn lại:
            =>Cách đọc = số hàng chục + "mươi" + số đơn vị


4/ Trường hợp hàng trăm > 0:
    -Nếu hàng chục = 0:
        =>Cách đọc = số hàng trăm + "trăm" + "lẻ" + số đơn vị
    -Nếu hàng chục = 1:
        +Nếu đơn vị = 5
            =>Cách đọc = số hàng trăm + "Trăm" "Mười Lăm"
        +Còn lại:
            =>Cách đọc = số hàng trăm + "trăm" + "Mười" + số đơn vị
    -Nếu hàng chục > 0:
        +Nếu đơn vị = 5
            =>Cách đọc = số hàng trăm + "trăm" + số hàng chục + "mươi" + "lăm"
        +Nếu đơn vị = 1
            =>Cách đọc = số hàng trăm + "trăm" + số hàng chục + "mươi" + "mốt"        
        +Còn lại:
            =>Cách đọc = số hàng trăm + "trăm" + số hàng chục + "mươi" + số đơn vị

*/
function cachDoc(chuso) {
  switch (chuso) {
    case 1: {
      return "một";
    }
    case 2: {
      return "hai";
    }
    case 3: {
      return "ba";
    }
    case 4: {
      return "bốn";
    }
    case 5: {
      return "năm";
    }
    case 6: {
      return "sáu";
    }
    case 7: {
      return "bảy";
    }
    case 8: {
      return "tám";
    }
    case 9: {
      return "chín";
    }
    case 0: {
      return "";
    }
  }
}

function doc3ChuSo() {
  var so3ChuSo = document.getElementById("so3ChuSo").value;

  if (so3ChuSo > 999 || so3ChuSo <= 0) {
    document.getElementById("cachDoc3ChuSo").innerHTML = "Số không hợp lệ";
    return;
  }

  var tram = Math.floor(so3ChuSo / 100);
  var chuc = Math.floor((so3ChuSo - tram * 100) / 10);
  var donVi = so3ChuSo - tram * 100 - chuc * 10;
  var cachDocSo;

  if (tram == 0) {
    switch (chuc) {
      case 0: {
        cachDocSo = cachDoc(donVi);
        break;
      }
      case 1: {
        if (donVi == 5) {
          cachDocSo = "mười lăm";
        } else {
          cachDocSo = "mười " + cachDoc(donVi);
        }
        break;
      }
      default: {
        if (donVi == 5) {
          cachDocSo = cachDoc(chuc) + " mươi lăm";
        } else if (donVi == 1) {
          cachDocSo = cachDoc(chuc) + " mươi mốt";
        } else {
          cachDocSo = cachDoc(chuc) + " mươi " + cachDoc(donVi);
        }
        break;
      }
    }
  } else {
    switch (chuc) {
      case 0: {
        if (donVi == 0) {
          cachDocSo = cachDoc(tram) + " trăm";
        } else {
          cachDocSo = cachDoc(tram) + " trăm lẻ " + cachDoc(donVi);
        }
        break;
      }
      case 1: {
        if (donVi == 5) {
          cachDocSo = cachDoc(tram) + " trăm mười lăm";
        } else {
          cachDocSo = cachDoc(tram) + " trăm mười " + cachDoc(donVi);
        }
        break;
      }
      default: {
        if (donVi == 5) {
          cachDocSo = cachDoc(tram) + " trăm " + cachDoc(chuc) + " mươi lăm";
        } else if (donVi == 1) {
          cachDocSo = cachDoc(tram) + " trăm " + cachDoc(chuc) + " mươi mốt";
        } else {
          cachDocSo =
            cachDoc(tram) +
            " trăm " +
            cachDoc(chuc) +
            " mươi " +
            cachDoc(donVi);
        }
        break;
      }
    }
  }

  document.getElementById("cachDoc3ChuSo").innerHTML = cachDocSo;
}
/*
Bài 4:
Input: tọa độ trường, tọa độ nhà 3 sinh viên
Output: nhà sinh viên xa trường nhất
Step:
1/ tạo function tính khoảng cách = công thức tính tọa độ giữa 2 điểm (https://mobitool.net/cong-thuc-khoang-cach.html)
2/ tạo các biến chứa tọa độ
3/ tính toán khoảng cách bằng function ở trên và so sánh:
  - Nếu khoảng cách của 1 sv lớn hơn 2 sv còn lại => sv đó xa nhất
  - Nếu khoảng cách của cả 3 sv bằng nhau => nhà của cả 3 sv xa bằng nhau
  - Nếu khoảng cách của nhà 2 sv bằng nhau và lớn hơn sv còn lại => nhà 2 sv đó xa nhất.
*/
function doKhoangCach(x1, y1, x2, y2) {
  var khoangCach = Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
  if (khoangCach < 0) {
    khoangCach *= -1;
  }
  return khoangCach;
}

function kiemTraSVXaTruongNhat() {
  var toaDoX0 = document.getElementById("toaDoX0").value * 1;
  var toaDoY0 = document.getElementById("toaDoY0").value * 1;
  var toaDoX1 = document.getElementById("toaDoX1").value * 1;
  var toaDoY1 = document.getElementById("toaDoY1").value * 1;
  var toaDoX2 = document.getElementById("toaDoX2").value * 1;
  var toaDoY2 = document.getElementById("toaDoY2").value * 1;
  var toaDoX3 = document.getElementById("toaDoX3").value * 1;
  var toaDoY3 = document.getElementById("toaDoY3").value * 1;

  var SVXaNhat = "";

  var khoangCachSV1 = doKhoangCach(toaDoX0, toaDoY0, toaDoX1, toaDoY1);
  var khoangCachSV2 = doKhoangCach(toaDoX0, toaDoY0, toaDoX2, toaDoY2);
  var khoangCachSV3 = doKhoangCach(toaDoX0, toaDoY0, toaDoX3, toaDoY3);

  if (khoangCachSV1 > khoangCachSV2 && khoangCachSV1 > khoangCachSV3) {
    SVXaNhat = "Nhà sinh viên 1 xa nhất";
  } else if (khoangCachSV2 > khoangCachSV1 && khoangCachSV2 > khoangCachSV3) {
    SVXaNhat = "Nhà sinh viên 2 xa nhất";
  } else if (khoangCachSV3 > khoangCachSV1 && khoangCachSV3 > khoangCachSV2) {
    SVXaNhat = "Nhà sinh viên 3 xa nhất";
  } else if (khoangCachSV1 == khoangCachSV2 && khoangCachSV1 == khoangCachSV3) {
    SVXaNhat = "Nhà của cả 3 sinh viên xa bằng nhau";
  } else if (khoangCachSV1 == khoangCachSV2 && khoangCachSV1 > khoangCachSV3) {
    SVXaNhat = "Nhà của sinh viên 1 và 2 xa nhất";
  } else if (khoangCachSV1 == khoangCachSV3 && khoangCachSV1 > khoangCachSV2) {
    SVXaNhat = "Nhà của sinh viên 1 và 3 xa nhất";
  } else {
    SVXaNhat = "Nhà của sinh viên 2 và 3 xa nhất";
  }

  document.getElementById("xaTruongNhat").innerHTML = SVXaNhat;
}
