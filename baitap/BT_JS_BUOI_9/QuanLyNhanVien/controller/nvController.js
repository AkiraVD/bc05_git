function layThongTinTuForm() {
  var taiKhoan = document.getElementById("tknv").value;
  var tenNV = document.getElementById("name").value;
  var email = document.getElementById("email").value;
  var matKhau = document.getElementById("password").value;
  var ngayLam = document.getElementById("datepicker").value;
  var luongCB = document.getElementById("luongCB").value;
  var chucVu = document.getElementById("chucvu").value;
  var gioLam = document.getElementById("gioLam").value;

  var nv = new NhanVien(
    taiKhoan,
    tenNV,
    email,
    matKhau,
    ngayLam,
    luongCB,
    chucVu,
    gioLam
  );
  return nv;
}

function showThongTinLenTable(dsnv) {
  var contentHTML = "";
  dsnv.forEach((item) => {
    contentHTML += `
    <tr>
        <td>${item.tk}</td>
        <td>${item.ten}</td>
        <td>${item.email}</td>
        <td>${item.ngay}</td>
        <td>${item.cv}</td>
        <td>${formatter.format(item.tongLuong())}</td>
        <td>${item.xeploai()}</td>
        <td><button class="btn btn-danger my-1" onclick="xoaNV('${
          item.tk
        }')"><i class="fa fa-times my-1"></i></button>
        <button class="btn btn-warning" onclick="suaNV('${
          item.tk
        }' )"data-toggle="modal" data-target="#myModal"><i class="fa fa-undo"></i></button></td>
    </tr>`;
  });
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

function timViTri(taiKhoan, dsnv) {
  for (var index = 0; index < dsnv.length; index++) {
    if (taiKhoan == dsnv[index].tk) {
      return index;
    }
  }
  return -1;
}

function showThongTinLenForm(viTri, dsnv) {
  document.getElementById("tknv").value = dsnv[viTri].tk;
  document.getElementById("name").value = dsnv[viTri].ten;
  document.getElementById("email").value = dsnv[viTri].email;
  document.getElementById("password").value = dsnv[viTri].pass;
  document.getElementById("datepicker").value = dsnv[viTri].ngay;
  document.getElementById("luongCB").value = dsnv[viTri].luong;
  document.getElementById("chucvu").value = dsnv[viTri].cv;
  document.getElementById("gioLam").value = dsnv[viTri].gio;
}
