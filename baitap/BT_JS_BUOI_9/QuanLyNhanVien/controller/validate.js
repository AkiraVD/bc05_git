function kiemTraRong(input, id, messenge) {
  if (input == "") {
    document.getElementById(id).innerHTML = messenge;
    document.getElementById(id).style.display = "contents";
    return false;
  } else {
    document.getElementById(id).innerHTML = "";
    document.getElementById(id).style.display = "none";
    return true;
  }
}

function kiemTraDinhDang(input, RegExp, id, messenge) {
  if (RegExp.test(input)) {
    document.getElementById(id).innerHTML = "";
    document.getElementById(id).style.display = "none";
    return true;
  } else {
    document.getElementById(id).innerHTML = messenge;
    document.getElementById(id).style.display = "contents";
    return false;
  }
}

function kiemTraDoDai(input, min, max, id, messenge) {
  if (input.length < min || input.length > max) {
    document.getElementById(id).innerHTML = messenge;
    document.getElementById(id).style.display = "contents";
    return false;
  } else {
    document.getElementById(id).innerHTML = "";
    document.getElementById(id).style.display = "none";
    return true;
  }
}

function kiemTraNamTrongKhoang(input, min, max, id, messenge) {
  if (isNaN(input) || input < min || input > max) {
    document.getElementById(id).innerHTML = messenge;
    document.getElementById(id).style.display = "contents";
    return false;
  } else {
    document.getElementById(id).innerHTML = "";
    document.getElementById(id).style.display = "none";
    return true;
  }
}

function kiemTraTrungTK(input, Array, id, messenge) {
  for (var i = 0; i < Array.length; i++) {
    if (input == Array[i].tk) {
      document.getElementById(id).innerHTML = messenge;
      document.getElementById(id).style.display = "contents";
      return false;
    }
  }
  document.getElementById(id).innerHTML = "";
  document.getElementById(id).style.display = "none";
  return true;
}

// Function kiểm tra hợp lệ (dãy, có kiểm tra trùng hay không: true,false)
function kiemTraHopLe(arr, ktTrung) {
  var isValid = true;
  // Validate tài khoản
  var RegexNumb = /^\d+$/; // Regex chỉ chứa chữ số
  isValid =
    kiemTraRong(arr.tk, "tbTKNV", "Tài khoản không được để trống") &&
    kiemTraDoDai(arr.tk, 4, 6, "tbTKNV", "Tài khoản tối đa 4 - 6 ký số") &&
    kiemTraDinhDang(
      arr.tk,
      RegexNumb,
      "tbTKNV",
      "Tài khoản tối đa 4 - 6 ký số"
    );
  if (ktTrung) {
    isValid =
      isValid && kiemTraTrungTK(arr.tk, dsnv, "tbTKNV", "Tài khoản đã tồn tại");
  }

  //Validate Tên NV
  var RegexTen = /^[a-zA-Z ]+$/;
  isValid =
    isValid &
    (kiemTraRong(arr.ten, "tbTen", "Tên nhân viên không được để trống") &&
      kiemTraDinhDang(arr.ten, RegexTen, "tbTen", "Tên nhân viên phải là chữ"));

  //Validate Email
  var RegexEmail =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  isValid =
    isValid &
    (kiemTraRong(arr.email, "tbEmail", "Email không được để trống") &&
      kiemTraDinhDang(
        arr.email,
        RegexEmail,
        "tbEmail",
        "Email chưa đúng định dạng"
      ));

  //Validate Password
  var RegexPass = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/;
  isValid =
    isValid &
    (kiemTraRong(arr.pass, "tbMatKhau", "Password không được để trống") &&
      kiemTraDinhDang(
        arr.pass,
        RegexPass,
        "tbMatKhau",
        "Password từ 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)"
      ));

  //Validate ngày
  var RegexDate =
    /^((0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01])[- /.](19|20)?[0-9]{2})*$/;
  isValid =
    isValid &
    (kiemTraRong(arr.ngay, "tbNgay", "Ngày không được để trống") &&
      kiemTraDinhDang(
        arr.ngay,
        RegexDate,
        "tbNgay",
        "Ngày chưa đúng định dạng mm/dd/yyyy"
      ));

  //Validate Lương
  isValid =
    isValid &
    (kiemTraRong(arr.luong, "tbLuongCB", "Lương không được để trống") &&
      kiemTraNamTrongKhoang(
        arr.luong,
        1000000,
        20000000,
        "tbLuongCB",
        "Lương nhập trong khoảng từ 1.000.000 đến 20.000.000"
      ));

  //Validate Chức vụ
  switch (arr.cv) {
    case "Sếp":
    case "Trưởng phòng":
    case "Nhân viên":
      document.getElementById("tbChucVu").innerHTML = "";
      document.getElementById("tbChucVu").style.display = "none";
      break;
    default:
      isValid = isValid & false;
      document.getElementById("tbChucVu").innerHTML =
        "Xin hãy chọn chức vụ hợp lệ";
      document.getElementById("tbChucVu").style.display = "contents";
      break;
  }

  //Validate Giờ làm
  isValid =
    isValid &
    (kiemTraRong(arr.gio, "tbGiolam", "Giờ làm không được để trống") &&
      kiemTraNamTrongKhoang(
        arr.gio,
        80,
        200,
        "tbGiolam",
        "Giờ làm nhập trong khoảng từ 80 đến 200 giờ"
      ));
  return isValid;
}
