/**
 * Bài 1: Tính lương nhân viên
 *
 * Input: Số ngày làm
 *
 * Step:
 * 1/ Tạo biến Số ngày làm, hằng số Lương 1 ngày, biến số Lương tháng
 * 2/ Lương tháng = số ngày làm * lương 1 ngày
 *
 * Output: Lương tháng
 */
var days;
var salaryPerDay;
var totalSalary;

function calSalary() {
  days = document.getElementById("workDays").value;
  salaryPerDay = document.getElementById("salaryPerDay").value;
  totalSalary = days * salaryPerDay;

  document.getElementById("totalSalary").innerHTML = new Intl.NumberFormat(
    "vn-VN",
    { style: "currency", currency: "VND" }
  ).format(totalSalary);
}
// 24 * 100000 = 2400000

/**
 * Bài 2: Tính giá trị trung bình
 *
 * Input: 5 số thực
 *
 * Step:
 * 1/ tạo 5 biến cho 5 số thực, biến giá trị trung bình
 * 2/ Giá trị trung bình = (tổng 5 số thực)/5
 *
 * Output: Giá trị trung bình
 */

var number1;
var number2;
var number3;
var number4;
var number5;
var averageValue = 0;

function calAverage() {
  number1 = document.getElementById("number1").value;
  number2 = document.getElementById("number2").value;
  number3 = document.getElementById("number3").value;
  number4 = document.getElementById("number4").value;
  number5 = document.getElementById("number5").value;
  var sum = ~~number1 + ~~number2 + ~~number3 + ~~number4 + ~~number5;
  averageValue = sum / 5;

  document.getElementById("averageValue").innerHTML = averageValue;
}
/** Bài 3: Quy đổi tiền
 *
 * Input: Số tiền = USD
 *
 * Step:
 * 1/ tạo biến số tiền = USD, số tiền = VND, tý giá.
 * 2/ Số tiền VND = số tiền USD * tỷ giá
 *
 * Output: Số tiền = VND
 */

var usdMoney;
var usdToVnd = 23500;
var vndMoney;

document.getElementById("usdToVnd").innerHTML = new Intl.NumberFormat("vn-VN", {
  style: "currency",
  currency: "VND",
}).format(usdToVnd);

vndMoney = usdMoney * usdToVnd;

function exchangeCurrency() {
  usdMoney = document.getElementById("usdMoney").value;
  vndMoney = usdMoney * usdToVnd;
  document.getElementById("vndMoney").innerHTML = new Intl.NumberFormat(
    "vn-VN",
    { style: "currency", currency: "VND" }
  ).format(vndMoney);
}
/** Bài 4: Diện tích, chu vi hình chữ nhật
 *
 * Input: Chiều dài, chiều rộng
 *
 * Step:
 * 1/ tạo biến Chiều dài, chiều rộng, chu vi, diện tích
 * 2/ Chu vi = (chiều dài + chiều rộng) * 2
 * 3/ diện tích = chiều dài * chiều rộng
 *
 * Output: Chu vi, diện tích
 */

var recH;
var recW;
var perimeter;
var area;

function recCal() {
  recH = document.getElementById("recH").value;
  recW = document.getElementById("recW").value;
  perimeter = (~~recH + ~~recW) * 2;
  area = recH * recW;
  document.getElementById("perimeter").innerHTML = perimeter;
  document.getElementById("area").innerHTML = area;
}

/** Bài 5: Tổng 2 ký số
 *
 * Input: 1 số có 2 chữ số
 *
 * Step:
 * 1/ tạo biến chứa số có 2 chữ số, số hàng đơn vị, số hàng chục, tổng 2 ký số
 * 2/ Số hàng đơn vị = số lẻ khi số đó chia cho 10
 * 3/ Số hàng chục = lấy số đó chia 10 rồi làm tròn xuống
 * 4/ tổng 2 ký số = số hàng chục + số hàng đơn vị
 * Output: Tổng 2 ký số
 */

var number2unit;
var tenth;
var unit;
var total;

function sumUnitNumber() {
  number2unit = document.getElementById("number2unit").value;
  unit = number2unit % 10;
  tenth = Math.floor(number2unit / 10);
  total = unit + tenth;

  document.getElementById("total").innerHTML = total;
}
