var arrayNumber = [];
function inputNumber() {
  var numb = document.querySelector("#txt-number").value * 1;
  arrayNumber.push(numb);

  document.querySelector("#arrayNumber").innerHTML = "Dãy số: " + arrayNumber;

  document.querySelector("#txt-number").value = "";
}
// bài 1
function tinhTongSoDuong() {
  var soDuong = 0;
  var sumSoDuong = 0;

  for (let i = 0; i < arrayNumber.length; i++) {
    soDuong = arrayNumber[i];
    if (soDuong > 0) {
      sumSoDuong += soDuong;
    }
  }

  document.querySelector("#tongSoDuong").innerHTML = sumSoDuong;
}

// Bài 2
function demSoDuong() {
  var soDuong = 0;
  var count = 0;

  for (let i = 0; i < arrayNumber.length; i++) {
    soDuong = arrayNumber[i];
    if (soDuong > 0) {
      count++;
    }
  }

  document.querySelector("#soDuong").innerHTML = count;
}

//Bài 3
function timSoNhoNhat() {
  var numb = 0;
  var smallest = arrayNumber[0];

  for (let i = 0; i < arrayNumber.length; i++) {
    numb = arrayNumber[i];

    if (smallest > numb) {
      smallest = numb;
    }
  }

  document.querySelector("#soNhoNhat").innerHTML = smallest;
}

//Bài 4
function timSoDuongNhoNhat() {
  var posSmallest = arrayNumber[0];

  for (let i = 0; i < arrayNumber.length; i++) {
    var numb = arrayNumber[i];

    if (numb > 0 && posSmallest > numb) {
      posSmallest = numb;
    }
  }

  document.querySelector("#soDuongNhoNhat").innerHTML = posSmallest;
}

//BÀi 5
function timSoChanCuoiCung() {
  var soChanCuoiCung = -1;
  for (let i = 0; i < arrayNumber.length; i++) {
    var numb = arrayNumber[i];
    if (numb % 2 == 0) {
      soChanCuoiCung = numb;
    }
  }
  document.querySelector("#soChanCuoiCung").innerHTML = soChanCuoiCung;
}

//Bài 6
function doiCho() {
  var index1 = document.querySelector("#index1").value * 1;
  var index2 = document.querySelector("#index2").value * 1;
  var value1 = arrayNumber[index1];
  var value2 = arrayNumber[index2];
  arrayNumber[index1] = value2;
  arrayNumber[index2] = value1;

  document.querySelector("#newArray").innerHTML = "Dãy số mới: " + arrayNumber;
}

//Bài 7
function sapXepTangDan() {
  arrayNumber.sort(function (a, b) {
    return a - b;
  });

  document.querySelector("#arraySorted").innerHTML =
    "Dãy số đã sắp xếp: " + arrayNumber;
}

//Bài 8
function checkPrime(x) {
  if (x < 2) return false;
  var y = 0;
  for (var i = 1; i <= x; i++) {
    if (x % i == 0) {
      y++;
      if (y > 2) {
        return false;
      }
    }
  }
  return true;
}
function timSoNguyenToDauTien() {
  var prime = -1;
  for (let i = 0; i < arrayNumber.length; i++) {
    if (checkPrime(arrayNumber[i])) {
      prime = arrayNumber[i];
      break;
    }
  }

  document.querySelector("#soNguyenToDauTien").innerHTML = prime;
}

//Bài 9
var realArrayNumber = [];
function demSoThuc() {
  var numb = document.querySelector("#txt-number-2").value * 1;
  realArrayNumber.push(numb);
  document.querySelector("#realArrayNumber").innerHTML =
    "Dãy số: " + realArrayNumber;
  document.querySelector("#txt-number-2").value = "";

  var count = 0;
  for (let i = 0; i < realArrayNumber.length; i++) {
    var numb = realArrayNumber[i];
    if (numb - Math.floor(numb) == 0) {
      count++;
    }
  }

  document.querySelector("#soThuc").innerHTML = "Số lượng số nguyên: " + count;
}

//BÀi 10
function soSanhAmDuong() {
  var soAm = 0;
  var soDuong = 0;

  for (let i = 0; i < arrayNumber.length; i++) {
    if (arrayNumber[i] > 0) {
      soDuong++;
    } else if (arrayNumber[i] < 0) {
      soAm++;
    }
  }

  document.querySelector(
    "#KQSoSanh"
  ).innerHTML = `Số lượng số dương: ${soDuong} <br/>
  Số lượng số âm: ${soAm}`;
}
