// Bài 1
var sum = 0;
var n = 0;
do {
  sum += n;
  n++;
} while (sum < 10000);
console.log("sum: ", sum);

document.getElementById(
  "soNguyenNhoNhat"
).innerHTML = `Số nguyên nhỏ nhất là: ${n}`;

// Bài 2
function tinhTong() {
  var x = document.getElementById("txt-x").value * 1;
  var n = document.getElementById("txt-n").value * 1;
  var xPowerN = 1;
  var sum = 0;

  for (var i = 1; i <= n; i++) {
    xPowerN *= x;
    sum += xPowerN;
    console.log(`x^${i} = ${xPowerN}`);
  }
  console.log("x^n = ", xPowerN);

  document.getElementById("tinhTong").innerHTML = sum;
}

//Bài 3
function tinhGiaiThua() {
  var x = document.getElementById("txt-numb3").value * 1;
  var giaiThua = 0;
  for (var i = 0; i <= x; i++) {
    giaiThua += i;
  }
  document.getElementById("giaiThua").innerHTML = giaiThua;
}

//Bài 4
function inDiv() {
  var content = ``;
  for (var i = 1; i <= 10; i++) {
    if (i % 2 == 0) {
      content += `<div class="bg-danger text-light pl-2">Div chẵn ${i}</div>`;
    } else {
      content += `<div class="bg-primary text-light pl-2">Div lẻ ${i}</div>`;
    }
  }
  console.log(content);
  document.getElementById("inDiv").innerHTML = content;
}
