/*
 Bài 5
Input: 1 số bất kì
Output: dãy số nguyên tố từ 1 tới số đã nhập
Step:
  1/ tạo các biên chứa giá trị input (n), string chứa dãy số nguyên tố
  2/ Tạo function để biết số x có phải số nguyên tố hay không:
    -tạo biến y là tổng số các số mà x chia hết.
    -Tạo 1 vòng lặp từ 1 tới x, mỗi lần x chia hết cho 1 số nào đó thì y += 1. 
    -Số nguyên tố là số chỉ chia hết cho 1 và x => x là số nguyên tố khi y == 2.
  3/ Cho function vào vòng lặp, mỗi lần gặp số nguyên tố thì add số đó vào dãy số. 
*/
function checkPrime(x) {
  var y = 0;
  for (var i = 1; i <= x; i++) {
    if (x % i == 0) {
      y++;
      if (y > 2) {
        return false;
      }
    }
  }
  return true;
}
function inSoNguyenTo() {
  var n = document.getElementById("txt-number").value;
  var daySoNguyenTo = "2";

  for (var i = 3; i <= n; i++) {
    if (checkPrime(i)) {
      daySoNguyenTo += " " + i;
    }
  }

  document.getElementById("tongSoNguyenTo").innerHTML = daySoNguyenTo;
}
