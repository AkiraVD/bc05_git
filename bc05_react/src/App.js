import "./App.css";
import Ex_Form from "./Ex_Form/Ex_Form";
// import LifeCircle from "./LifeCircle/LifeCircle";
// import Ex_Tai_Xiu from "./Ex_Tai_Xiu/Ex_Tai_Xiu";
// import Ex_ShoeShop_Redux from "./Ex_ShoeShop_Redux/Ex_ShoeShop_Redux";
// import Demo_Mini_Redux from "./Demo_Mini_Redux/Demo_Mini_Redux";
// import Ex_ShoeShop from "./Ex_ShoeShop/Ex_ShoeShop";
// import Ex_car from "./Ex_car/Ex_car";
// import logo from "./logo.svg";
// import DemoClass from "./DemoComponent/DemoClass";
// import DemoFunction from "./DemoComponent/DemoFunction";
// import Ex_Layout from "./Ex_Layout/Ex_Layout";
// import RenderWithMap from "./Ex_Layout/RenderWithMap/RenderWithMap";
// import DataBiding from "./DataBiding/DataBiding";
// import DemoProbs from "./DemoProps/DemoProps";
// import DemoState from "./DemoState/DemoState";
// import ConditionalRendering from "./ConditionalRendering/ConditionalRendering";

function App() {
  return (
    <div className="App">
      {/* <DemoClass />
      <DemoFunction /> */}
      {/* <Ex_Layout /> */}
      {/* <DataBiding /> */}
      {/* <DemoProbs /> */}
      {/* <RenderWithMap /> */}
      {/* <DemoState /> */}
      {/* <Ex_car /> */}
      {/* <ConditionalRendering /> */}
      {/* <Ex_ShoeShop /> */}
      {/* <Demo_Mini_Redux /> */}
      {/* <Ex_ShoeShop_Redux /> */}
      {/* <Ex_Tai_Xiu /> */}
      {/* <LifeCircle /> */}
      <Ex_Form />
    </div>
  );
}

export default App;
