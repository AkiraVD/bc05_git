import { combineReducers } from "redux";
import { numberReducer } from "./numberReducer";

export const rootReducer_Demo_mini = combineReducers({
  numberReducer,
});
