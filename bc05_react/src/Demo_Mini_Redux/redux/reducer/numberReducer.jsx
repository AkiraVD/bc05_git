let innitialState = {
  number: 100,
};

export const numberReducer = (state = innitialState, action) => {
  switch (action.type) {
    case "TANG_SO_LUONG": {
      state.number += action.payload;
      return { ...state };
    }
    case "GIAM_SO_LUONG": {
      state.number -= action.payload;
      return { ...state };
    }
    default:
      return state;
  }
};
