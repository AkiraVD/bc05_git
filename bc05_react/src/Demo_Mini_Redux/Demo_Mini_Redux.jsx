import React, { Component } from "react";
import { connect } from "react-redux";

class Demo_Mini_Redux extends Component {
  render() {
    return (
      <div>
        <button
          onClick={() => {
            this.props.handleGiamSoLuong(5);
          }}
          className="btn btn-danger"
        >
          Giam
        </button>
        <span className="display-4 mx-5">{this.props.soLuong}</span>
        <button
          onClick={() => {
            this.props.handleTangSoLuong(5);
          }}
          className="btn btn-success"
        >
          Tang
        </button>
      </div>
    );
  }
}

let mapState2Props = (state) => {
  return {
    soLuong: state.numberReducer.number,
  };
};

let mapDispatch2Props = (dispatch) => {
  return {
    handleTangSoLuong: (value) => {
      let action = {
        type: "TANG_SO_LUONG",
        payload: value,
      };
      dispatch(action);
    },
    handleGiamSoLuong: (value) => {
      dispatch({
        type: "GIAM_SO_LUONG",
        payload: value,
      });
    },
  };
};

export default connect(mapState2Props, mapDispatch2Props)(Demo_Mini_Redux);
