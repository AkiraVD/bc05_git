import React, { Component } from "react";
import UserForm from "./UserForm";
import UserTable from "./UserTable";

export default class Ex_Form extends Component {
  state = {
    userList: [
      {
        id: 1,
        name: "John",
        password: "124",
      },
    ],
    userEditing: null,
  };
  handleAddUser = (newUser) => {
    let cloneUserList = [...this.state.userList, newUser];
    this.setState({ userList: cloneUserList });
  };
  handleDeleteUser = (userid) => {
    let index = this.state.userList.findIndex((item) => {
      return item.id === userid;
    });
    if (index !== -1) {
      let cloneUserList = [...this.state.userList];
      cloneUserList.splice(index, 1);
      this.setState({ userList: cloneUserList });
    }
  };
  handleEditUser = (value) => {
    this.setState({ userEditing: value });
  };
  render() {
    return (
      <div className="container py-5">
        <UserForm
          handleAddUser={this.handleAddUser}
          userEditing={this.state.userEditing}
        />
        <UserTable
          userList={this.state.userList}
          handleDeleteUser={this.handleDeleteUser}
          handleEditUser={this.handleEditUser}
        />
      </div>
    );
  }
}
