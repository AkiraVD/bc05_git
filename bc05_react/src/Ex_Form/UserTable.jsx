import React, { Component } from "react";

export default class UserTable extends Component {
  renderUserTable() {
    return this.props.userList.map((item, key) => {
      return (
        <tr key={key}>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.password}</td>
          <td>
            <button
              className="btn btn-danger me-2"
              onClick={() => {
                this.props.handleDeleteUser(item.id);
              }}
            >
              Del
            </button>
            <button
              className="btn btn-warning"
              onClick={() => {
                this.props.handleEditUser(item);
              }}
            >
              Edit
            </button>
          </td>
        </tr>
      );
    });
  }
  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Password</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>{this.renderUserTable()}</tbody>
        </table>
      </div>
    );
  }
}
