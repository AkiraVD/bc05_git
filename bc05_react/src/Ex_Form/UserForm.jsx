import { nanoid } from "nanoid";
import React, { Component } from "react";

export default class UserForm extends Component {
  constructor(props) {
    super(props);
    this.inputRef = React.createRef();
  }
  state = {
    user: {
      name: "",
      password: "",
    },
  };
  componentWillReceiveProps(nextProps, nextContext) {
    if (nextProps.userEditing !== null) {
      this.setState({
        user: {
          name: nextProps.userEditing.name,
          password: nextProps.userEditing.password,
        },
      });
    }
  }
  componentDidMount() {
    // this.inputRef.current.value = "Alice";
    // this.inputRef.current.style.color = "red";
  }
  handleGetUserForm = (event) => {
    console.log(event.target.value);
    let { value, name: key } = event.target; // name : key -> dđổi tên name thành key
    let cloneUser = { ...this.state.user };
    cloneUser[key] = value;

    this.setState(
      {
        user: cloneUser,
      },
      () => {
        console.log(this.state.user);
      }
    );
  };
  handleSummitForm = () => {
    let newUser = { ...this.state.user };
    newUser.id = nanoid(5);
    this.props.handleAddUser(newUser);
  };
  render() {
    return (
      <div>
        <div className="mb-3">
          <input
            onChange={this.handleGetUserForm}
            value={this.state.user.name}
            ref={this.inputRef}
            type="text"
            className="form-control"
            name="name"
            placeholder="Username"
          />
        </div>
        <div className="mb-3">
          <input
            onChange={this.handleGetUserForm}
            value={this.state.user.password}
            type="text"
            className="form-control"
            name="password"
            placeholder="Password"
          />
        </div>
        <button className="btn btn-warning" onClick={this.handleSummitForm}>
          Add User
        </button>
      </div>
    );
  }
}
