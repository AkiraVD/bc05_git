import React, { Component } from "react";

export default class DetailShoe extends Component {
  render() {
    let detail = this.props.detail;
    return (
      <div className="row text-start border border-primary">
        <img src={detail.image} alt="" className="col-4" />
        <div className="col-8 d-flex flex-column justify-content-center border border-info">
          <p>Description: {detail.description}</p>
          <p>Price: {detail.price}</p>
          <p>Qty: {detail.quantity}</p>
        </div>
      </div>
    );
  }
}
