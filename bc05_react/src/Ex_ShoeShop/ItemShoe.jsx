import React, { Component } from "react";

export default class ItemShoe extends Component {
  render() {
    let { id, image, name } = this.props.data;
    return (
      <div key={id} className="col-3 p-1">
        <div className="card text-start h-100">
          <img className="card-img-top" src={image} alt="Title" />
          <div className="card-body">
            <h5 className="card-title">{name}</h5>
            <br />
            <button
              className="btn btn-warning"
              onClick={() => {
                this.props.handleAdd2Cart(this.props.data);
              }}
            >
              Buy
            </button>
            <button
              className="btn btn-primary"
              onClick={() => {
                this.props.handleViewDetails(this.props.data);
              }}
            >
              Detail
            </button>
          </div>
        </div>
      </div>
    );
  }
}
