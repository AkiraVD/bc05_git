import React, { Component } from "react";
import Cart from "./Cart";
import { dataShoe } from "./dataShoe";
import DetailShoe from "./DetailShoe";
import ListShoe from "./ListShoe";

export default class Ex_ShoeShop extends Component {
  state = {
    shoeArr: dataShoe,
    detail: dataShoe[0],
    cart: [],
  };
  handleChangeDetails = (value) => {
    this.setState({
      detail: value,
    });
  };
  handleAdd2Cart = (shoe) => {
    let cartItem = { ...shoe, number: 1 };
    let cloneCart = [...this.state.cart];

    for (let i = 0; i < cloneCart.length; i++) {
      if (shoe.id === cloneCart[i].id) {
        cloneCart[i].number++;
        this.setState({ cart: cloneCart });
        return;
      }
    }

    cloneCart.push(cartItem);

    this.setState({ cart: cloneCart });
  };

  handleAddNumber = (index) => {
    let cloneCart = [...this.state.cart];
    cloneCart[index].number++;
    this.setState({ cart: cloneCart });
  };

  handleReduceNumber = (index) => {
    let cloneCart = [...this.state.cart];
    cloneCart[index].number--;
    if (cloneCart[index].number === 0) {
      cloneCart.splice(index, 1);
    }
    this.setState({ cart: cloneCart });
  };
  render() {
    return (
      <div className="container">
        <Cart
          cart={this.state.cart}
          handleAddNumber={this.handleAddNumber}
          handleReduceNumber={this.handleReduceNumber}
        />
        <ListShoe
          shoeArr={this.state.shoeArr}
          handleChangeDetails={this.handleChangeDetails}
          handleAdd2Cart={this.handleAdd2Cart}
        />
        <DetailShoe detail={this.state.detail} />
      </div>
    );
  }
}
