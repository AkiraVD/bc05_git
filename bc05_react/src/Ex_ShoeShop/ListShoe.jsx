import React, { Component } from "react";
import ItemShoe from "./ItemShoe";

export default class ListShoe extends Component {
  renderListShoe = () => {
    return this.props.shoeArr.map((item) => {
      return (
        <ItemShoe
          data={item}
          handleViewDetails={this.props.handleChangeDetails}
          handleAdd2Cart={this.props.handleAdd2Cart}
        />
      );
    });
  };

  render() {
    return <div className="row">{this.renderListShoe()}</div>;
  }
}
