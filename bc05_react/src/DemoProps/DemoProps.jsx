import React, { Component } from "react";
import UserInfo from "./UserInfo";

export default class DemoProbs extends Component {
  renderTitle = () => {
    return <h1>Nice to meet you</h1>;
  };

  render() {
    let user = {
      name: "Bob",
      gmail: "bob@gmail.com",
    };
    return (
      <div>
        <h2>DemoProbs</h2>
        <UserInfo renderContent={this.renderTitle} data={user} />
      </div>
    );
  }
}
