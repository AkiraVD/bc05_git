import React, { Component } from "react";

export default class UserInfo extends Component {
  handleClickMe = () => {
    console.log("Yes");
  };
  render() {
    console.log(this.props);
    return (
      <div>
        <h2>Username : {this.props.data.name}</h2>
        <h2>Gmail: {this.props.data.gmail}</h2>
        <button className="btn btn-success" onClick={this.handleClickMe}>
          Click me
        </button>
        {this.props.renderContent()}
      </div>
    );
  }
}
