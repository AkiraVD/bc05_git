import React, { Component } from "react";

export default class DemoState extends Component {
  state = {
    like: 0,
  };
  handlePlusLike = () => {
    // setState: dùng khi muốn thay đổi giao diện trong render
    // setState ~ Bất đồng bộ => chạy sau cùng
    this.setState(
      {
        like: this.state.like + 1,
      },
      () => {
        console.log("thành công", this.state.like);
      }
    );
  };
  render() {
    return (
      <div>
        <p className="display-4">{this.state.like}</p>
        <button className="btn btn-primary" onClick={this.handlePlusLike}>
          Like
        </button>
      </div>
    );
  }
}
