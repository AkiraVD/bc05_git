import React, { Component } from "react";
import { connect } from "react-redux";

class Result extends Component {
  render() {
    return (
      <div
        className="mt-5 fs-2 fw-bold"
        style={{ textShadow: "0px 0px 5px #FFFFFF" }}
      >
        <p className="text-danger ">{this.props.result}</p>
        BẠN CHỌN: {this.props.choice} <br />
        Tổng số lần Bạn Thắng: {this.props.win} <br />
        Tổng số lần Tung Xúc Xắc: {this.props.total} <br />
      </div>
    );
  }
}

let mapState2Props = (state) => {
  return {
    result: state.XucXacReducer.result,
    choice: state.XucXacReducer.choice,
    win: state.XucXacReducer.win,
    total: state.XucXacReducer.total,
  };
};

export default connect(mapState2Props)(Result);
