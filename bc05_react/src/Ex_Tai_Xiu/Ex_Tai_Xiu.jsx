import React, { Component } from "react";
import GameBody from "./GameBody";
import Result from "./Result";
import bg_game from "../assets/bgGame.png";
import "./game.css";

export default class Ex_Tai_Xiu extends Component {
  render() {
    return (
      <div
        className="font_game"
        style={{
          backgroundImage: `url(${bg_game})`,
          width: "100vw",
          height: "100vh",
        }}
      >
        <div className="container">
          <h2 className="py-5 fw-bold fs-1">GAME XÚC SẮC</h2>
          <GameBody />
          <Result />
        </div>
      </div>
    );
  }
}
