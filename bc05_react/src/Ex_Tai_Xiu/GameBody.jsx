import React, { Component } from "react";
import { connect } from "react-redux";
import { CHOOSE_TAI_SUU, PLAY_GAME, TAI, XIU } from "./redux/xucSacConstant";

class GameBody extends Component {
  handleXucXac = () => {
    let data = this.props.mangXucXac;
    return data.map((item) => {
      return (
        <div style={{ width: "100px" }}>
          <img style={{ width: "100%" }} src={item.img} alt="" />
        </div>
      );
    });
  };
  render() {
    return (
      <div>
        <div className="d-flex justify-content-evenly align-items-center">
          <button
            className="btn btn-danger fw-bold fs-1 text-warning "
            style={{ width: "100px", height: "100px" }}
            onClick={() => {
              this.props.handleChoose(TAI);
            }}
          >
            TÀI
          </button>
          <div className="d-flex justify-content-center">
            {this.handleXucXac()}
          </div>
          <button
            className="btn btn-dark fw-bold fs-1  text-warning"
            style={{ width: "100px", height: "100px" }}
            onClick={() => {
              this.props.handleChoose(XIU);
            }}
          >
            XỈU
          </button>
        </div>
        <button
          className="btn btn-success fw-bold fs-3 mt-4"
          onClick={this.props.handlePlay}
        >
          Play Game
        </button>
      </div>
    );
  }
}

let mapState2Props = (state) => {
  return {
    mangXucXac: state.XucXacReducer.mangXucXac,
  };
};
let mapDispatch2Props = (dispatch) => {
  return {
    handleChoose: (luaChon) => {
      dispatch({
        type: CHOOSE_TAI_SUU,
        payload: luaChon,
      });
    },
    handlePlay: () => {
      dispatch({
        type: PLAY_GAME,
      });
    },
  };
};

export default connect(mapState2Props, mapDispatch2Props)(GameBody);
