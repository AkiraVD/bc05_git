import React, { Component } from "react";

export default class Ex_car extends Component {
  state = {
    img: "./img/CarBasic/products/black-car.jpg",
  };
  handleChangeColor = (color) => {
    this.setState({
      img: `./img/CarBasic/products/${color}-car.jpg`,
    });
  };
  render() {
    return (
      <div className="container py-5">
        <div className="row">
          <img src={this.state.img} alt="" className="col-4" />
          <div>
            <button
              className="btn btn-dark"
              onClick={() => {
                this.handleChangeColor("black");
              }}
            >
              Black
            </button>
            <button
              className="btn btn-danger mx-4"
              onClick={() => {
                this.handleChangeColor("red");
              }}
            >
              Red
            </button>
            <button
              className="btn btn-outline-secondary"
              onClick={() => {
                this.handleChangeColor("silver");
              }}
            >
              Silver
            </button>
            <button
              className="btn btn-secondary mx-4"
              onClick={() => {
                this.handleChangeColor("steel");
              }}
            >
              Steel
            </button>
          </div>
        </div>
      </div>
    );
  }
}
