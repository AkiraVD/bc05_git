import React, { Component } from "react";

export default class ConditionalRendering extends Component {
  state = {
    isLogin: false,
  };
  handleLog = (mode) => {
    this.setState({
      isLogin: mode,
    });
  };
  renderContent = () => {
    if (this.state.isLogin) {
      // đã đăng nhập
      return (
        <div>
          <p>Hi Alice</p>
          <button
            className="btn btn-danger"
            onClick={() => {
              this.handleLog(false);
            }}
          >
            Log out
          </button>
        </div>
      );
    } else {
      // chưa đăng nhập
      return (
        <div>
          <p>Welcome, Please login</p>
          <button
            className="btn btn-danger"
            onClick={() => {
              this.handleLog(true);
            }}
          >
            Log in
          </button>
        </div>
      );
    }
  };
  render() {
    return <div>{this.renderContent()}</div>;
  }
}
