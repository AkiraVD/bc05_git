import React, { PureComponent } from "react";

export default class Header extends PureComponent {
  // PureComponent sẽ không rander lại
  componentDidMount() {
    let time = 300;
    this.countdown = setInterval(() => {
      console.log("countdown", time--);
    }, 1000);
  }
  render() {
    console.log("HEADER render");
    return <div className="p-5 bg-dark text-white">Header</div>;
  }

  componentWillUnmount() {
    console.log("HEADER unmounted");
    clearInterval(this.countdown);
  }
}
