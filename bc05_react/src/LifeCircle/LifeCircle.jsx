import React, { Component } from "react";
import Header from "./Header";

export default class LifeCircle extends Component {
  state = {
    like: 1,
  };
  componentDidMount() {
    console.log("LIFE_CIRCLE componentDidMount");
    // MOUNTING chỉ dc chạy 1 lần duy nhất (sau khi render chạy)
    // Thường để gọi API
  }
  handlePlusLike = () => {
    this.setState({
      like: this.state.like + 1,
    });
  };
  shouldComponentUpdate(nextProps, nextState) {
    console.log("nextProps: ", nextProps);
    console.log("nextState: ", nextState);
    // mặc định sẽ return -> true
    if (nextState.like === 2) {
      console.log("LIFE_CIRCLE not render");
      return false;
    }
    return true;
  }
  render() {
    console.log("LIFE_CIRCLE render");
    return (
      <div className="p-5 bg-primary text-white">
        {this.state.like < 5 && <Header />}
        <p>LifeCircle</p>
        <span className="display-4">{this.state.like}</span> <br />
        <button onClick={this.handlePlusLike} className="btn btn-success">
          Plus like
        </button>
      </div>
    );
  }
  componentDidUpdate() {
    console.log("LIFE_CIRCLE componentDidUpdate");
  }
}
