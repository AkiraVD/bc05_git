import React, { Component } from "react";

export default class CardItem extends Component {
  render() {
    return (
      <div className="card text-start h-100">
        <img
          className="card-img-top"
          src={this.props.movie.hinhAnh}
          alt="Title"
        />
        <div className="card-body h-50 overflow-hidden">
          <h4 className="card-title">{this.props.movie.tenPhim}</h4>
          <p className="card-text">{this.props.movie.moTa}</p>
        </div>
      </div>
    );
  }
}
