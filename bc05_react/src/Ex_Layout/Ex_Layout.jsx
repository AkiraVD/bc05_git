import React, { Component } from "react";
import Content from "./Content";
import Footer from "./Footer";
import Header from "./Header";
import Navigate from "./Navigate";

export default class Ex_Layout extends Component {
  render() {
    return (
      <div>
        <Header />
        <div className="row">
          <div className="col-3 p-0 m-0">
            <Navigate />
          </div>
          <div className="col-9 p-0 m-0">
            <Content />
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}
