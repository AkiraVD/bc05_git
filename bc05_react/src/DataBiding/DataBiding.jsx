import React, { Component } from "react";

export default class DataBiding extends Component {
  imgSrc =
    "https://image.tmdb.org/t/p/w220_and_h330_face/pIkRyD18kl4FhoCNQuWxWu5cBLM.jpg";
  renderDescription = () => {
    return <div>Hello to Allice</div>;
  };
  render() {
    let username = "Alice";
    return (
      <div>
        <div
          style={{ width: "200px", backgroundColor: "red", color: "white" }}
          className="card text-start"
        >
          <img className="card-img-top" src={this.imgSrc} alt="Title" />
          <div className="card-body">
            <h4 className="card-title">{username}</h4>
            <p className="card-text">{this.renderDescription()}</p>
          </div>
        </div>
      </div>
    );
  }
}
