export const ADD_TO_CART = "ADD_TO_CART";
export const REDUCE_NUMBER = "REDUCE_NUMBER";
export const ADD_NUMBER = "ADD_NUMBER";
export const CHANGE_DETAIL = "CHANGE_DETAIL";
