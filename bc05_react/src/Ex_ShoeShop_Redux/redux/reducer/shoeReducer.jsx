import { dataShoe } from "../../dataShoe";
import {
  ADD_NUMBER,
  ADD_TO_CART,
  CHANGE_DETAIL,
  REDUCE_NUMBER,
} from "../constant/shoeConstant";

let innitialState = {
  shoeArr: dataShoe,
  detail: dataShoe[0],
  cart: [],
};

export const shoeReducer = (state = innitialState, action) => {
  switch (action.type) {
    case CHANGE_DETAIL: {
      state.detail = action.payload;
      return { ...state };
    }

    case ADD_TO_CART: {
      let cloneCart = [...state.cart];
      let addItem = action.payload;
      let addItemIndex = cloneCart.findIndex((item) => {
        return item.id === addItem.id;
      });

      if (addItemIndex === -1) {
        cloneCart.push({ ...addItem, number: 1 });
      } else {
        cloneCart[addItemIndex].number++;
      }

      return { ...state, cart: cloneCart };
    }

    case REDUCE_NUMBER: {
      let index = action.payload;
      let cloneCart = [...state.cart];
      cloneCart[index].number--;
      if (cloneCart[index].number === 0) {
        cloneCart.splice(index, 1);
      }
      return { ...state, cart: cloneCart };
    }

    case ADD_NUMBER: {
      let index = action.payload;
      let cloneCart = [...state.cart];
      cloneCart[index].number++;
      return { ...state, cart: cloneCart };
    }

    default:
      return state;
  }
};
