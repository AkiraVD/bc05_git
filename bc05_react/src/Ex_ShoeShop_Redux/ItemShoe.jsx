import React, { Component } from "react";
import { connect } from "react-redux";
import { ADD_TO_CART, CHANGE_DETAIL } from "./redux/constant/shoeConstant";

class ItemShoe extends Component {
  render() {
    let { id, image, name } = this.props.data;
    return (
      <div key={id} className="col-3 p-1">
        <div className="card text-start h-100">
          <img className="card-img-top" src={image} alt="Title" />
          <div className="card-body">
            <h5 className="card-title">{name}</h5>
            <br />
            <button
              className="btn btn-warning"
              onClick={() => {
                this.props.handleAdd2Cart(this.props.data);
              }}
            >
              Buy
            </button>
            <button
              className="btn btn-primary"
              onClick={() => {
                this.props.handleChangeDetails(this.props.data);
              }}
            >
              Detail
            </button>
          </div>
        </div>
      </div>
    );
  }
}

let mapDispatch2Props = (dispatch) => {
  return {
    handleAdd2Cart: (shoe) => {
      let action = {
        type: ADD_TO_CART,
        payload: shoe,
      };
      dispatch(action);
    },
    handleChangeDetails: (shoe) => {
      let action = {
        type: CHANGE_DETAIL,
        payload: shoe,
      };
      dispatch(action);
    },
  };
};

export default connect(null, mapDispatch2Props)(ItemShoe);
