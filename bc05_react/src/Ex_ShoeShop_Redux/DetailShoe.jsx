import React, { Component } from "react";
import { connect } from "react-redux";

class DetailShoe extends Component {
  render() {
    let detail = this.props.detail;
    return (
      <div className="row text-start border border-primary">
        <img src={detail.image} alt="" className="col-4" />
        <div className="col-8 d-flex flex-column justify-content-center border border-info">
          <p>Description: {detail.description}</p>
          <p>Price: {detail.price}</p>
          <p>Qty: {detail.quantity}</p>
        </div>
      </div>
    );
  }
}

let mapState2Props = (state) => {
  return {
    detail: state.shoeReducer.detail,
  };
};

export default connect(mapState2Props)(DetailShoe);
