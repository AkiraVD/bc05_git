import React, { Component } from "react";
import { connect } from "react-redux";
import ItemShoe from "./ItemShoe";

class ListShoe extends Component {
  renderListShoe = () => {
    let list = this.props.list;
    return list.map((item) => {
      return <ItemShoe data={item} />;
    });
  };

  render() {
    return <div className="row">{this.renderListShoe()}</div>;
  }
}

let mapState2Props = (state) => {
  return {
    list: state.shoeReducer.shoeArr,
  };
};

export default connect(mapState2Props)(ListShoe);
