import React, { Component } from "react";
import { connect } from "react-redux";
import { ADD_NUMBER, REDUCE_NUMBER } from "./redux/constant/shoeConstant";

class Cart extends Component {
  renderTbody = () => {
    return this.props.gioHang.map((item, index) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price * item.number}</td>
          <td>
            <button
              onClick={() => {
                this.props.handleReduceNumber(index);
              }}
            >
              -
            </button>
            {item.number}
            <button
              onClick={() => {
                this.props.handleAddNumber(index);
              }}
            >
              +
            </button>
          </td>
          <td>
            <img style={{ width: "80px" }} src={item.image} alt="" />
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <table className="table">
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Price</th>
            <th>Quantity</th>
            <th>Image</th>
          </tr>
        </thead>
        <tbody>{this.renderTbody()}</tbody>
      </table>
    );
  }
}

let mapState2Props = (state) => {
  return {
    gioHang: state.shoeReducer.cart,
  };
};

let mapDispatch2Props = (dispatch) => {
  return {
    handleReduceNumber: (index) => {
      let action = {
        type: REDUCE_NUMBER,
        payload: index,
      };
      dispatch(action);
    },
    handleAddNumber: (index) => {
      let action = {
        type: ADD_NUMBER,
        payload: index,
      };
      dispatch(action);
    },
  };
};

export default connect(mapState2Props, mapDispatch2Props)(Cart);
