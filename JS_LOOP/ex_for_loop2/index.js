function inTongSoChan() {
  var number = document.getElementById("txt-number").value * 1;
  var sum = 0;

  for (var i = 1; i <= number; i++) {
    if (i % 2 == 0) {
      sum += i;
    }
  }

  document.getElementById(
    "result"
  ).innerHTML = `Tổng các số chẵn từ 1 đến ${number} là ${sum}`;
}

function tinhTongSoChiaHetCho3() {
  var number3 = document.getElementById("txt-number-3").value * 1;

  var tongCacSoChiaHetCho3 = 0;
  for (var i = 0; i <= number3; i++) {
    if (i % 3 == 0) {
      tongCacSoChiaHetCho3++;
    }
  }

  document.getElementById(
    "result_3"
  ).innerHTML = `Tổng các số chia hết cho 3 từ 0 tới ${number3} là ${tongCacSoChiaHetCho3}`;
  console.log("tongCacSoChiaHetCho3: ", tongCacSoChiaHetCho3);
}
