function inKQ() {
  var soChan = 0;
  var soLe = 0;
  var stringChan = "";
  var stringLe = "";

  /*
    for (let i = 0; i < 101; i++) {
      if (i % 2 == 0) {
        soChan++;
        stringChan += " " + i;
      } else {
        soLe++;
        stringLe += " " + i;
      }
    }
    */
  //while loop
  var count = 0;
  while (count <= 100) {
    if (count % 2 == 0) {
      soChan++;
      stringChan += " " + count;
    } else {
      soLe++;
      stringLe += " " + count;
    }
    count++;
  }

  document.getElementById(
    "result"
  ).innerHTML = `Số Chẵn: ${stringChan} </br> Số lẻ: ${stringLe} `;
}
