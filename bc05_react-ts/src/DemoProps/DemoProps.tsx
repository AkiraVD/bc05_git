import React from "react";
import UserInfo from "./UserInfo";

type Props = {};

export interface UserData {
  name: string;
  age: number;
}
let data: UserData = {
  name: "Alice",
  age: 2,
};

export default function DemoProps({}: Props) {
  return (
    <div>
      <UserInfo user={data} />
    </div>
  );
}
