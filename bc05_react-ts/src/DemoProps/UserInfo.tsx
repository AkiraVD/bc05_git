import React from "react";
import { UserData } from "./DemoProps";

interface UserProps {
  user: UserData;
}

export default function UserInfo(Props: UserProps) {
  return (
    <div className="alert alert-primary">
      <h3>
        Name : <span>{Props.user.name}</span> <br />
        Age : <span>{Props.user.age}</span>
      </h3>
    </div>
  );
}
