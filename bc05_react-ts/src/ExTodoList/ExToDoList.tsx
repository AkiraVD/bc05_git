import React, { useState } from "react";
import { InterfaceTodo } from "./Interface/interfaceExTodoList";
import TodoForm from "./TodoForm/TodoForm";
import TodoList from "./TodoList/TodoList";

type Props = {};

export default function ExToDoList({}: Props) {
  const [todos, setTodos] = useState<InterfaceTodo[]>([
    { id: "1", title: "Make capstone web", isCompleted: false },
    { id: "2", title: "Buy new phone", isCompleted: true },
    { id: "3", title: "Sell house", isCompleted: false },
  ]);
  const handleAddTodo = (todo: InterfaceTodo) => {
    let newTodos = [...todos, todo];
    setTodos(newTodos);
  };
  const handleRemoveTodo = (idTodo: string) => {
    let newTodos = todos.filter((item) => {
      return item.id !== idTodo;
    });
    setTodos(newTodos);
  };
  return (
    <div className="container py-5">
      <TodoForm handleAddTodo={handleAddTodo} />
      <TodoList data={todos} handleRemoveTodo={handleRemoveTodo} />
    </div>
  );
}
