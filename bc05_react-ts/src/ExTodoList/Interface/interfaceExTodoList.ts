export interface InterfaceTodo {
  id: string;
  title: string;
  isCompleted: boolean;
}

export interface InterfaceTodoListComponent {
  data: InterfaceTodo[];
  handleRemoveTodo: (id: string) => void;
}

export interface InterfaceTodoItemComponent {
  item: InterfaceTodo;
  handleRemoveTodo: (id: string) => void;
}

export interface InterfaceTodoFormComponent {
  handleAddTodo: (value: InterfaceTodo) => void;
}
