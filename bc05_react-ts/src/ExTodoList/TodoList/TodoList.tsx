import React from "react";
import { InterfaceTodoListComponent } from "../Interface/interfaceExTodoList";
import TodoItem from "./TodoItem";

type Props = {};

export default function TodoList({
  data,
  handleRemoveTodo,
}: InterfaceTodoListComponent) {
  const renderDataList = () => {
    return data.map((item) => {
      return <TodoItem item={item} handleRemoveTodo={handleRemoveTodo} />;
    });
  };
  return (
    <div className="mt-3">
      <div className="table-responsive">
        <table className="table table-hover">
          <thead className="table-primary">
            <tr>
              <th scope="col">ID</th>
              <th scope="col">Title</th>
              <th scope="col">Completed</th>
            </tr>
          </thead>
          <tbody>{renderDataList()}</tbody>
        </table>
      </div>
    </div>
  );
}
