import React, { useState } from "react";
import { customAlphabet, nanoid } from "nanoid";

import {
  InterfaceTodo,
  InterfaceTodoFormComponent,
} from "../Interface/interfaceExTodoList";

type Props = {};

export default function TodoForm({
  handleAddTodo,
}: InterfaceTodoFormComponent) {
  const [title, setTitle] = useState<string>("");
  const handleOnChangeTitle = (event: React.ChangeEvent<HTMLInputElement>) => {
    setTitle(event.target.value);
  };
  const nanoid = customAlphabet("1234567890", 10);
  const handleSummit = () => {
    if (title == "") {
      return;
    }
    let objectTodo: InterfaceTodo = {
      id: nanoid(3),
      title: title,
      isCompleted: false,
    };
    handleAddTodo(objectTodo);
    setTitle("");
  };
  return (
    <div>
      <div className="mb-3">
        <input
          value={title}
          type="text"
          className="form-control"
          placeholder="Title"
          onChange={handleOnChangeTitle}
        />
      </div>
      <button className="btn btn-warning" onClick={handleSummit}>
        Add Todo
      </button>
    </div>
  );
}
