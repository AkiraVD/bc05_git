import React from "react";
import logo from "./logo.svg";
import "./App.css";
import DemoProps from "./DemoProps/DemoProps";
import ExToDoList from "./ExTodoList/ExToDoList";

function App() {
  return (
    <div className="App">
      <ExToDoList />
    </div>
  );
}

export default App;
