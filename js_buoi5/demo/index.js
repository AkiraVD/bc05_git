//định nghĩa
function sayHello() {
  //hàm không có tham số
  console.log("Hello");
  console.log("Have a good day");
}

// thực thi

sayHello();
sayHello();
sayHello();
sayHello();
sayHello();

function sayHelloSpe(username) {
  //hàm có tham số
  //username : param ~ ko cần khai báo var
  console.log("Chào", username);
}

sayHelloSpe("Alice");
sayHelloSpe("Bob");

function tinhDTB(toan, ly, hoa, username) {
  //hàm có nhiều tham số
  var dtb = (toan + ly + hoa) / 3;
  console.log("Chào ", username);
  console.log(`Điểm trung bình của ${username} là: ${dtb} `);
  // chỉ được return về 1 giá trị
  return dtb;
}

// 5, 6, 7, "Alice" là argument
var sv1 = tinhDTB(5, 6, 7, "Alice");
var sv2 = tinhDTB(9, 9, 9, "Bob");

console.log("sv1: ", sv1);
console.log("sv2: ", sv2);
