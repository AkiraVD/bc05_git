const UBER_CAR = "uberCar";
const UBER_SUV = "uberSUV";
const UBER_BLACK = "uberBlack";

function tinhGiaTienKmDau(car) {
  if (car == UBER_CAR) {
    return 8000;
  } else if (car == UBER_SUV) {
    return 9000;
  } else {
    return 10000;
  }
}

function tinhGiaTienKm1den19(car) {
  if (car == UBER_CAR) {
    return 7500;
  } else if (car == UBER_SUV) {
    return 8500;
  } else {
    return 9500;
  }
}

function tinhGiaTienKm19trolen(car) {
  if (car == UBER_CAR) {
    return 7000;
  } else if (car == UBER_SUV) {
    return 8000;
  } else {
    return 9000;
  }
}

function tinhGiaTienCho(car) {
  switch (car) {
    case UBER_CAR: {
      return 2000;
    }
    case UBER_SUV: {
      return 3000;
    }
    case UBER_BLACK: {
      return 3500;
    }
  }
}
//main
function tinhTienUber() {
  console.log("Test");
  var loaiXe = document.querySelector('input[name="selector"]:checked').value;

  var giaTienKmDau = tinhGiaTienKmDau(loaiXe);

  var giaTienKm1den19 = tinhGiaTienKm1den19(loaiXe);

  var giaTienKm19trolen = tinhGiaTienKm19trolen(loaiXe);

  var SoKm = document.getElementById("txt-km").value * 1;

  var tgCho = document.getElementById("txt-tg").value * 1;

  var soLanCho = Math.floor(tgCho / 3);

  var giaTienCho = tinhGiaTienCho(loaiXe);

  var tongTien = 0;

  if (tgCho >= 3) {
    tienCho = giaTienCho * soLanCho;
  }

  if (SoKm <= 1) {
    tongTien = giaTienKmDau * SoKm + tienCho;
  } else if (SoKm <= 19) {
    tongTien = giaTienKmDau + (SoKm - 1) * giaTienKm1den19 + tienCho;
  } else {
    tongTien =
      giaTienKmDau +
      18 * giaTienKm1den19 +
      (SoKm - 19) * giaTienKm19trolen +
      tienCho;
  }

  document.getElementById("divThanhTien").style.display = "block";
  document.getElementById("xuatTien").innerText = tongTien + "vnđ";
}
