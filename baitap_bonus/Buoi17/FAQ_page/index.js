// Tạo dãy các thẻ tab
const tab = document.querySelectorAll(".tab");

for (var i = 0; i < tab.length; i++) {
  // biến element bằng giá trị thẻ tab tại vị trí i
  const element = tab[i];

  // biến btn bằng giá trị thẻ tab tại i và lấy class plus
  const btn = tab[i].querySelector(".plus");

  // add event onclick vào thẻ có class plus (biến btn)
  // khi click thì biến element sẽ bằng tab[i]
  btn.onclick = function () {
    handleCollapse(element);
  };
  console.log("btn: ", btn);
}

function handleCollapse(element) {
  for (var i = 0; i < tab.length; i++) {
    if (element !== tab[i]) {
      tab[i].classList.remove("show-text");
    }
  }
  console.log("tab: ", tab);
  element.classList.toggle("show-text");
  console.log("element: ", element);
}
