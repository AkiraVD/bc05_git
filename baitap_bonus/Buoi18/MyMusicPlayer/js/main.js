//Song lists
var song1 = {
  name: "Lemon",
  artist: "Kenshi Yonezu",
  src: "./musics/Lemon.mp3",
  img: "./thumbnails/Lemon.jpg",
};
var song2 = {
  name: "I Really Want to Stay at Your House",
  artist: "Rosa Walton",
  src: "./musics/I_Really_Want_to_Stay_at_Your_House.mp3",
  img: "./thumbnails/I_Really_Want_to_Stay_at_Your_House.jpg",
};
var song3 = {
  name: "Cry Baby",
  artist: "Ofcial HiGE DANdism",
  src: "./musics/CryBaby.mp3",
  img: "./thumbnails/CryBaby.jpg",
};
var song4 = {
  name: "Pretender",
  artist: "Ofcial HiGE DANdism",
  src: "./musics/Pretender.mp3",
  img: "./thumbnails/Pretender.jpg",
};
var song5 = {
  name: "Free Bird",
  artist: "Lynyrd Skynyrd",
  src: "./musics/FreeBird.mp3",
  img: "./thumbnails/FreeBird.jpg",
};
var songList = [song1, song2, song3, song4, song5];

//Lấy thông tin từ website
let nowPlaying = document.querySelector("#music_player .nowPlaying");
let thumbnail = document.querySelector("#music_player .thumbnail");
let songName = document.querySelector("#music_player .songName");
let artist = document.querySelector("#music_player .artist");

let playBtn = document.querySelector("#music_player .btn-play");
let backwardBtn = document.querySelector("#music_player .btn-backward");
let forwardBtn = document.querySelector("#music_player .btn-forward");
let randomBtn = document.querySelector("#music_player .btn-random");
let loopBtn = document.querySelector("#music_player .btn-loop");

let timeSlider = document.querySelector("#music_player .timeSlider");
let volumeBar = document.querySelector("#music_player .volumeBar");
let currentTime = document.querySelector("#music_player .currentTime");
let songDuration = document.querySelector("#music_player .songDuration");

// Các giá trị chung
let trackIndex = -1;
let isPlaying = false;
let updateTimer;

//Thay đổi các giá trị về ban đầu
function resetValue() {
  currentTime.textContent = "00:00";
  songDuration.textContent = "00:00";
  timeSlider.value = 0;
}

// Tạo element audio cho music player
let currentTrack = document.createElement("audio");
console.log("currentTrack: ", currentTrack.volume);

function seekTo() {
  //Tính toán vị trí trên thanh thời gian
  let time = currentTrack.duration * (timeSlider.value / 100);

  //Chỉnh vị trí hiện tại về vị trí đã tính toán
  currentTrack.currentTime = time;
}

function setVolume() {
  //Chỉnh volume dựa theo bar
  currentTrack.volume = volumeBar.value / 100;
  console.log("currentTrack.volume: ", currentTrack.volume);
}

function seekUpdate() {
  let seekPosition = 0;

  // Kiểm tra độ dài có phải số nguyên không
  if (!isNaN(currentTrack.duration)) {
    seekPosition = currentTrack.currentTime * (100 / currentTrack.duration);
    timeSlider.value = seekPosition;

    // Kiểm tra thời gian còn lại và tổng thời gian
    let currentMinutes = Math.floor(currentTrack.currentTime / 60);
    let currentSeconds = Math.floor(
      currentTrack.currentTime - currentMinutes * 60
    );
    let durationMinutes = Math.floor(currentTrack.duration / 60);
    let durationSeconds = Math.floor(
      currentTrack.duration - durationMinutes * 60
    );

    // Thêm số 0 vào đơn vị chỉ có 1 chữ số
    if (currentSeconds < 10) {
      currentSeconds = "0" + currentSeconds;
    }
    if (durationSeconds < 10) {
      durationSeconds = "0" + durationSeconds;
    }
    if (currentMinutes < 10) {
      currentMinutes = "0" + currentMinutes;
    }
    if (durationMinutes < 10) {
      durationMinutes = "0" + durationMinutes;
    }

    // Hiển thị và cập nhật thời gian
    currentTime.textContent = currentMinutes + ":" + currentSeconds;
    songDuration.textContent = durationMinutes + ":" + durationSeconds;
  }
}

// Load bài hát từ danh sách
function loadSong(trackIndex) {
  //Clear thời gian đang chọn cũ
  clearInterval(updateTimer);
  resetValue();

  //Load Bài mới
  currentTrack.volume = 0.2;
  currentTrack.src = songList[trackIndex].src;
  currentTrack.load();

  //Update Thông tin bài hát
  thumbnail.src = songList[trackIndex].img;
  songName.textContent = songList[trackIndex].name;
  artist.textContent = songList[trackIndex].artist;
  nowPlaying.textContent = `Playing song ${trackIndex + 1} of ${
    songList.length
  }`;

  // Chỉnh thời gian cập nhật timer
  updateTimer = setInterval(seekUpdate, 1000);

  // Tự động chỉnh qua bài mới khi kết thúc bài cũ
  currentTrack.addEventListener("ended", nextSong);
}

//Play bài hát

function playSong() {
  if (trackIndex == -1) {
    trackIndex = 0;
    loadSong(trackIndex);
  }
  setVolume();
  currentTrack.play();
  isPlaying = true;

  //Đổi icon
  document.querySelector("#music_player").classList.add("playing");
}

function pauseSong() {
  currentTrack.pause();
  isPlaying = false;

  //Đổi icon
  document.querySelector("#music_player").classList.remove("playing");
}

// Random between 2 value
function get2val(min, max) {
  return Math.random() * (max - min) + min;
}

function nextSong() {
  // Quay về bài đầu nếu bài hiện tại là bài cuối
  if (isRandom) {
    trackIndex = Math.floor(get2val(0, songList.length));
    loadSong(trackIndex);
    playSong();
    return;
  }
  if (trackIndex < songList.length - 1) trackIndex += 1;
  else if (!isLoop) {
    return;
  } else trackIndex = 0;
  loadSong(trackIndex);
  playSong();
}

function prevSong() {
  // Quay về bài cuối nếu bài hiện tại là bài đầu
  if (trackIndex > 0) trackIndex -= 1;
  else trackIndex = songList.length - 1;

  loadSong(trackIndex);
  playSong();
}

var isRandom = false;
function randomSong() {
  if (isRandom) {
    document
      .querySelector("#music_player .btn-random")
      .classList.remove("active");
    isRandom = false;
  } else {
    document.querySelector("#music_player .btn-random").classList.add("active");
    isRandom = true;
  }
}

var isLoop = false;
function loopSong() {
  if (isLoop) {
    document
      .querySelector("#music_player .btn-loop")
      .classList.remove("active");
    isLoop = false;
  } else {
    document.querySelector("#music_player .btn-loop").classList.add("active");
    isLoop = true;
  }
}
