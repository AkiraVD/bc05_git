// primitive type ~ string, number, boolean

// reference type ~ array, object

var menu = ["Bánh canh", "Bún bò", "Hủ tiếu"];
console.log("menu: ", menu);

// lấy số lượng hiện tại

var soLuong = menu.length;
console.log("soLuong: ", soLuong);

// thêm phần tử vào mảng
menu.push("Phở khô Gia Lai");
menu.push("Bún chả Hà Nội");
menu.push("Mì Quảng");

console.log("menu sau khi push: ", menu);
console.log("soLuong sau khi push: ", menu.length);

//duyệt mảng
for (var index = 0; index < menu.length; index++) {
  var monAnHienTai = menu[index];
  // index trong menu[index] là vị trí trong mảng
  console.log("monAnHienTai: ", monAnHienTai);
}

//Update giá trị của 1 phần tử trong array
menu[0] = "Bánh bèo";
console.log("menu sau khi update: ", menu);

// CRUD
// create read update delete

//forEach ~ duyệt mảng

menu.forEach(function (index, monAn) {
  console.log("monAn forEach: ", index, monAn);
});

//pop ~ delete phần tử cuối cùng trong mảng

console.log("menu truoc khi pop: ", menu);
menu.pop();
console.log("menu sau khi pop: ", menu);

var thongTin = "Bún bò";

//indexOf trả về vị trí ĐẦU TIÊN trong mảng (index) nếu tìm thấy, nếu không tìm thấy thì trả về -1
var indexThongTin = menu.indexOf(thongTin);
console.log("indexThongTin: ", indexThongTin);

if (indexThongTin !== -1) {
  menu[indexThongTin] = "Bánh bao";
}

console.log("menu sau khi update thông tin: ", menu);

//slice và splice
let indexBanhBao = menu.indexOf("Bánh bao");
//array.splice(vị trí, số lượng)
menu.splice(indexBanhBao, 1);
console.log("menu sau khi splice: ", menu);

//map

var thucDon = menu.map(function (monAn) {
  return "Món " + monAn;
});

console.log("menu: ", menu);
console.log("thucDon: ", thucDon);

let scores = [10, 10, 10, 10, 1, 6, 6, 7, 8, 2, 1, 1, 10];

let resultScores = scores.filter(function (number) {
  return number == 10;
});
console.log("resultScores: ", resultScores);
