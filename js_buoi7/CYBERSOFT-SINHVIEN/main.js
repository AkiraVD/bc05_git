var danhSachTheTr = document.querySelectorAll("#tblBody tr");

// Trả về điểm của 1 thẻ tr bất kì
var layDiemTuTheTr = function (trTag) {
  var danhSachTD = trTag.querySelectorAll("td");
  //Điểm là thẻ td thứ 4 từ trên xuống => đếm từ 0 thì index = 3
  return danhSachTD[3].innerHTML * 1;
};

// Trả về tên của 1 thẻ tr bất kì
var layTenTuTheTr = function (trTag) {
  var danhSachTD = trTag.querySelectorAll("td");
  //Điểm là thẻ td thứ 3 từ trên xuống => đếm từ 0 thì index = 2
  return danhSachTD[2].innerHTML;
};
// tìm sinh viên giỏi nhất
function timSvDiemCaoNhat() {
  var theTrLonNhat = danhSachTheTr[0];

  for (var index = 0; index < danhSachTheTr.length; index++) {
    var theTrHienTai = danhSachTheTr[index];
    var diemHienTai = layDiemTuTheTr(theTrHienTai);
    var diemLonNhat = layDiemTuTheTr(theTrLonNhat);

    if (diemHienTai > diemLonNhat) {
      theTrLonNhat = theTrHienTai;
    }
  }

  document.querySelector("#svGioiNhat").innerHTML = `Tên: ${layTenTuTheTr(
    theTrLonNhat
  )} <br/> Điểm trung bình: ${layDiemTuTheTr(theTrLonNhat)}`;
}

// tìm sinh viên có điểm thấp nhất
function timSvYeuNhat() {
  var theTrBeNhat = danhSachTheTr[0];

  for (var index = 0; index < danhSachTheTr.length; index++) {
    var theTrHienTai = danhSachTheTr[index];
    var diemHienTai = layDiemTuTheTr(theTrHienTai);
    var diemBeNhat = layDiemTuTheTr(theTrBeNhat);

    if (diemHienTai < diemBeNhat) {
      theTrBeNhat = theTrHienTai;
    }
  }

  document.querySelector("#svYeuNhat").innerHTML = `Tên: ${layTenTuTheTr(
    theTrBeNhat
  )} <br/> Điểm trung bình: ${layDiemTuTheTr(theTrBeNhat)}`;
}

// Tìm số sinh viên giỏi
function timSoSinhVienGioi() {
  var soSVGioi = 0;

  for (var index = 0; index < danhSachTheTr.length; index++) {
    var theTrHienTai = danhSachTheTr[index];
    var diemHienTai = layDiemTuTheTr(theTrHienTai);

    if (diemHienTai >= 8) {
      soSVGioi++;
    }
  }

  document.querySelector("#soSVGioi").innerHTML = soSVGioi;
}

// Danh sách sinh viên > 5
function TimDSDiemHon5() {
  var danhSachSVHon5 = [];

  for (var index = 0; index < danhSachTheTr.length; index++) {
    var theTrHienTai = danhSachTheTr[index];
    var diemHienTai = layDiemTuTheTr(theTrHienTai);

    if (diemHienTai > 5) {
      danhSachSVHon5.push(layTenTuTheTr(theTrHienTai));
    }
  }

  document.querySelector("#dsDiemHon5").innerHTML = danhSachSVHon5;
}

// Sắp xếp tăng
/*
 Cách 1 (Lỗi khi có người trùng điểm)
function layTheTrTuDiem(diemSo) {
  for (var i = 0; i < danhSachTheTr.length; i++) {
    var theTrHienTai = danhSachTheTr[i];
    if (layDiemTuTheTr(theTrHienTai) == diemSo) {
      return danhSachTheTr[i];
    }
  }
}
function sapXepTang() {
  var dsDiem = [];
  var dsTrTang = [];
  for (var index = 0; index < danhSachTheTr.length; index++) {
    var theTrHienTai = danhSachTheTr[index];
    console.log("theTrHienTai: ", theTrHienTai);

    dsDiem.push(layDiemTuTheTr(theTrHienTai));
  }
  dsDiem.sort(function (a, b) {
    return a - b;
  });
  for (var i = 0; i < dsDiem.length; i++) {
    dsTrTang.push(layTheTrTuDiem(dsDiem[i]));
  }

  var danhSachTang = "";
  for (var i = 0; i < dsTrTang.length; i++) {
    var theTrHienTai = dsTrTang[i];
    danhSachTang += `${i + 1}/ ${layTenTuTheTr(
      theTrHienTai
    )} - ${layDiemTuTheTr(theTrHienTai)} <br/> `;
  }

  document.querySelector("#dtbTang").innerHTML = danhSachTang;
}
*/

/* Cách 2 (Không sợ trùng điểm)
  Step:
  _Tạo 2 danh sách điểm và tên
  _Tạo hàm sort điểm từ bé đến lớn, khi sort điểm thì sort luôn tên theo index của điểm tại vì index của cả 2 trùng nhau
  -> Profit
*/
function sapXepTang() {
  var dsDiem = [];
  var dsTen = [];
  // array = [item1,item2,...]
  // forEach(function(item,index){ code function }
  // forEach(function(item){ code function }
  danhSachTheTr.forEach(function (theTr) {
    dsDiem.push(layDiemTuTheTr(theTr));
    dsTen.push(layTenTuTheTr(theTr));
  });

  for (var i = 0; i < dsDiem.length - 1; i++) {
    for (var j = i + 1; j < dsDiem.length; j++) {
      if (dsDiem[i] > dsDiem[j]) {
        // Hoán đổi thứ tự
        var temp1 = dsDiem[i];
        dsDiem[i] = dsDiem[j];
        dsDiem[j] = temp1;
        // Hoán đổi tên theo danh sách điểm
        var temp2 = dsTen[i];
        dsTen[i] = dsTen[j];
        dsTen[j] = temp2;
      }
    }
  }

  var danhSachTang = "";
  dsTen.forEach(function (tenSV, diemSo) {
    danhSachTang += tenSV + " - " + dsDiem[diemSo] + "<br/>";
  });

  //In Kết quả
  document.getElementById("dtbTang").innerHTML = danhSachTang;
}
