"use strict";
console.log("hello TS");
// Khi khai báo biến
//Primitive value: string, number, boolean, null, undefined
var username = "alice";
username = "bob";
// username = 3 // error
var age = 2;
// age = "2"
var isHoliday = true;
var isMarried = null;
var is_married = undefined;
var sv1 = {
    id: 1,
    name: "alice",
    age: 20,
};
var todo1 = {
    id: 1,
    title: "Lau nha",
    isFinished: false,
};
var todo2 = {
    id: 2,
    title: "nau com",
    isFinished: true,
    desc: "afasf",
};
var tivi = {
    id: 1,
    name: "Samsung OLED TV",
    price: 10,
};
// type array
var numberArr = [2, 4, 5, 6];
var gioiTinh = ["nam", "nu"];
var danhSachSanPham = [
    tivi,
    {
        id: 2,
        name: "Tu Lanh",
        price: 20,
    },
];
// type function
function tinhTong(numb1, numb2) {
    return numb1 + numb2;
}
function handleClick(value) {
    console.log(value);
}
function main(callback) {
    callback("hello world adsfasdf");
}
function renderSection(content) {
    var _a;
    (_a = document.getElementById("root")) === null || _a === void 0 ? void 0 : _a.innerHTML = `<h1 style="color:yellow;background:black">${content}</h1>`;
}
main(renderSection);
// tuple : array chứa nhiều loại dữ liệu khác nhau
var user = ["alice", 10];
var userAge = 2;
userAge = "2";
let todoDetails;
todoDetails = null;
todoDetails = {
    id: 1,
    title: "Lam du an",
    isFinished: false,
};
// enum type
var GenderType;
(function (GenderType) {
    GenderType["male"] = "001";
    GenderType["female"] = "101";
})(GenderType || (GenderType = {}));
var user1 = {
    id: 2,
    name: "Bob",
    gender: GenderType.male,
};
//generic
var nums = [2, 5, 7];
var strings = ["2", "alice"];
// function useState() {
//   let state: number | string;
//   function getState() {
//     return state;
//   }
//   function setState(x: number | string) {
//     state = x;
//   }
//   return {
//     getState,
//     setState,
//   };
// }
// let like_useState = useState();
// like_useState.setState("true");
function useState() {
    var state;
    function getState() {
        return state;
    }
    function setState(x) {
        state = x;
    }
    return {
        getState,
        setState,
    };
}
let like_useState = useState();
like_useState.setState(2);
