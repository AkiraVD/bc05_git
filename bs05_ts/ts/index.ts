console.log("hello TS");

// Khi khai báo biến

//Primitive value: string, number, boolean, null, undefined

var username: string = "alice";
username = "bob";
// username = 3 // error

var age = 2;
// age = "2"

var isHoliday: boolean = true;
var isMarried: null = null;
var is_married: undefined = undefined;

// reference value : object, array => interface
// dùng để mô tả kiểu dữ liệu của object

interface SinhVien {
  id: number;
  name: string;
  age: number;
}

var sv1: SinhVien = {
  id: 1,
  name: "alice",
  age: 20,
};

interface Todo {
  id: number;
  title: string;
  isFinished?: boolean; // thêm "?": optional, không cần thêm cũng dc
}

var todo1: Todo = {
  id: 1,
  title: "Lau nha",
  isFinished: false,
};

interface NewTodo extends Todo {
  desc: string;
}

var todo2: NewTodo = {
  id: 2,
  title: "nau com",
  isFinished: true,
  desc: "afasf",
};

// type: mô tả object, dùng để tạo ra type mới

type Product = {
  id: number;
  name: string;
  price: number;
};

var tivi: Product = {
  id: 1,
  name: "Samsung OLED TV",
  price: 10,
};

// type array

var numberArr: number[] = [2, 4, 5, 6];
var gioiTinh: string[] = ["nam", "nu"];

var danhSachSanPham: Product[] = [
  tivi,
  {
    id: 2,
    name: "Tu Lanh",
    price: 20,
  },
];

// type function
function tinhTong(numb1: number, numb2: number): number {
  return numb1 + numb2;
}

function handleClick(value: number): void {
  console.log(value);
}

function main(callback: (title: string) => void) {
  callback("hello world adsfasdf");
}

function renderSection(content: string) {
  document.getElementById(
    "root"
  )?.innerHTML = `<h1 style="color:yellow;background:black">${content}</h1>`;
}

main(renderSection);

// tuple : array chứa nhiều loại dữ liệu khác nhau
var user: [string, number] = ["alice", 10];

//union type :tạo ra type mới có khả năng nhận các loại dữ liệu khác nhau

type ResponseAgeBE = number | string;

var userAge: ResponseAgeBE = 2;
userAge = "2";

// userAge = true -> lỗi

type ResponseTodoBE = null | Todo;
let todoDetails: ResponseTodoBE;
todoDetails = null;
todoDetails = {
  id: 1,
  title: "Lam du an",
  isFinished: false,
};

// enum type
enum GenderType {
  male = "001",
  female = "101",
}

interface User {
  id: number;
  name: string;
  gender: GenderType;
}

var user1: User = {
  id: 2,
  name: "Bob",
  gender: GenderType.male,
};

//generic

var nums: Array<number> = [2, 5, 7];
var strings: Array<string> = ["2", "alice"];

// function useState() {
//   let state: number | string;
//   function getState() {
//     return state;
//   }
//   function setState(x: number | string) {
//     state = x;
//   }
//   return {
//     getState,
//     setState,
//   };
// }

// let like_useState = useState();
// like_useState.setState("true");

function useState<T>() {
  var state: T;
  function getState() {
    return state;
  }
  function setState(x: T) {
    state = x;
  }
  return {
    getState,
    setState,
  };
}
let like_useState = useState<number>();
like_useState.setState(2);
