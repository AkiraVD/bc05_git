var number = 1.5;

switch (number) {
  case 1: {
    console.log("Số 1");
    break;
  }
  case 2: {
    console.log("Số 2");
    break;
  }
  default: {
    console.log("Giá trị default");
  }
}
