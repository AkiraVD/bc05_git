var tienMuon;
var tienDaTra;
var tienConNo;
var tienPhat;
var PHAN_TRAM_LAI_SUAT = 0.015;

function tinhTienPhat() {
  tienMuon = document.getElementById("txt-tien-muon").value * 1;
  tienDaTra = document.getElementById("txt-tien-da-tra").value * 1;

  tienConNo = tienMuon - tienDaTra;
  if (tienConNo > 0) {
    tienPhat = tienConNo * PHAN_TRAM_LAI_SUAT;
  } else tienPhat = 0;

  document.getElementById("result").innerHTML = "Số tiền phạt: " + tienPhat;
}
