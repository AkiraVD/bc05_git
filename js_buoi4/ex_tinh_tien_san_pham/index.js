function tinhTien() {
  const soLuong = document.getElementById("txt-so-luong").value * 1;
  const donGia = document.getElementById("txt-don-gia").value * 1;

  var tongTien = 0;
  if (soLuong < 50) {
    tongTien = soLuong * donGia;
  } else if (soLuong <= 100) {
    tongTien = 49 * donGia + (soLuong - 49) * donGia * 0.92;
  } else {
    tongTien =
      49 * donGia + 51 * donGia * 0.92 + (soLuong - 100) * donGia * 0.88;
  }

  document.getElementById("result").innerHTML = tongTien;
}
