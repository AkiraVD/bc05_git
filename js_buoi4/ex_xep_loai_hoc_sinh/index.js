function xepLoai() {
  const diemToan = document.getElementById("txt-diem-toan").value * 1;
  const diemLy = document.getElementById("txt-diem-ly").value * 1;
  const diemHoa = document.getElementById("txt-diem-hoa").value * 1;

  var dtb = (diemToan + diemLy + diemHoa) / 3;
  var xepLoai;
  if (dtb >= 8.5) {
    console.log("Gioi");
    xepLoai = "Giỏi";
  } else if (dtb >= 6.5) {
    console.log("Kha");
    xepLoai = "Khá";
  } else if (dtb >= 5) {
    console.log("Trung Binh");
    xepLoai = "Trung Bình";
  } else {
    console.log("Yeu");
    xepLoai = "Yếu";
  }
  document.getElementById("result").innerHTML = `Điểm trung bình: ${dtb.toFixed(
    2
  )}
  <br/>
  Xếp loại: ${xepLoai}`;
}
