function layThongTinTuForm() {
  const ma = document.getElementById("txtMaSV").value;
  const ten = document.getElementById("txtTenSV").value;
  const loai = document.getElementById("loaiSV").value;
  const diemToan = document.getElementById("txtDiemToan").value * 1;
  const diemVan = document.getElementById("txtDiemVan").value * 1;

  var sv = {
    maSV: ma,
    loaiSV: loai,
    tenSV: ten,
    diemToanSV: diemToan,
    diemVanSV: diemVan,

    tinhDTB: function () {
      return (this.diemToanSV + this.diemVanSV) / 2;
    },

    xepLoai: function () {
      if (this.tinhDTB > 5) {
        return "Đạt";
      } else {
        return "Không đạt";
      }
    },
  };

  return sv;
}

function showThongTinLenForm(sv) {
  document.querySelector("#spanTenSV").innerHTML = sv.tenSV;
  document.querySelector("#spanMaSV").innerHTML = sv.maSV;
  document.querySelector("#spanLoaiSV").innerHTML = sv.loaiSV;
  document.querySelector("#spanDTB").innerHTML = sv.tinhDTB();
  document.querySelector("#spanXepLoai").innerHTML = sv.xepLoai();
}
