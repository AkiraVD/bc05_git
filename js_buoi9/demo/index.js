// Object = { key: value}
var sv1 = {
  name: "Alice",
  age: 2,
  gender: "female",
};
console.log("sv1 before: ", sv1);

var sv2 = {
  name: "Bob",
  age: 2,
  gender: "male",
};

sv1.age = 5;
console.log("sv1 after: ", sv1);

var cat1 = {
  name: "Black",
  score: 10,
  talk: function () {
    console.log("gâu gâu, I am", this.name);
  },
};

cat1.talk();

// Dynamic key
var key = "age";
cat1[key] = 20;

const cat2 = cat1;
console.log("cat1: ", cat1);
console.log("cat2: ", cat2);

cat2.score = 1;
console.log("After");
console.log("cat1: ", cat1);
console.log("cat2: ", cat2);
