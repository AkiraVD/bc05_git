//Lấy dữ liệu từ form
function layThongTinTuForm() {
  var maSV = document.querySelector("#txtMaSV").value;
  var tenSv = document.querySelector("#txtTenSV").value;
  var email = document.querySelector("#txtEmail").value;
  var matKhau = document.querySelector("#txtPass").value;
  var diemToan = document.querySelector("#txtDiemToan").value;
  var diemLy = document.querySelector("#txtDiemLy").value;
  var diemHoa = document.querySelector("#txtDiemHoa").value;

  var sv = new SinhVien(maSV, tenSv, email, matKhau, diemToan, diemLy, diemHoa);
  return sv;
}

//render danh sach sinh vien
function renderDssv(dssv) {
  var contentHTML = "";
  dssv.forEach(function (item) {
    var contentTr = `
    <tr>
        <td>${item.ma}</td>
        <td>${item.ten}</td>
        <td>${item.email}</td>
        <td>${item.tinhDTB().toFixed(1)}</td>
        <td>
            <button class="btn btn-danger" onclick="xoaSV('${
              item.ma
            }')">Xóa</button>
            <button class="btn btn-warning" onclick="suaSV('${
              item.ma
            }')">Sửa</button>
        </td>
    <tr>
    `;
    contentHTML += contentTr;
  });
  document.querySelector("#tbodySinhVien").innerHTML = contentHTML;

  // return contentHTML;
}

function timKiemViTri(id, dssv) {
  for (var index = 0; index < dssv.length; index++) {
    var item = dssv[index];
    //Nếu tìm thấy thì dừng function và trả về index hiện tại
    if (item.ma == id) {
      return index;
    }
  }
  //Tự quy định nếu không tìm  thấy thì trả về -1
  return -1;
}

function resetForm() {
  document.getElementById("formQLSV").reset();

  document.getElementById("spanMaSV").innerHTML = "";
  document.getElementById("spanTenSV").innerHTML = "";
  document.getElementById("spanEmailSV").innerHTML = "";
  document.getElementById("spanMatKhau").innerHTML = "";
  document.getElementById("spanToan").innerHTML = "";
  document.getElementById("spanLy").innerHTML = "";
  document.getElementById("spanHoa").innerHTML = "";
  //remove disable
  document.getElementById("txtMaSV").disabled = false;
}

function showMessengeErr(idErr, message) {
  document.getElementById(idErr).innerHTML = message;
}
