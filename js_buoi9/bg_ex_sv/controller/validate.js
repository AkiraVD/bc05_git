function kiemTratrung(sv, dssv) {
  let index = timKiemViTri(sv, dssv);
  if (index !== -1) {
    showMessengeErr("spanMaSV", "Mã này đã tồn tại");
    return false;
  } else {
    showMessengeErr("spanMaSV", "");
    return true;
  }
}

function kiemTraRong(userInput, idErr, message) {
  if (userInput.length == 0) {
    showMessengeErr(idErr, message);
    return false;
  } else {
    showMessengeErr(idErr, "");
    return true;
  }
}

function kiemTraSo(value, idErr, message) {
  var reg = /^\d+$/;
  let isNumber = reg.test(value);
  if (isNumber) {
    showMessengeErr(idErr, "");
    return true;
  } else {
    showMessengeErr(idErr, message);
    return false;
  }
}

function kiemTraEmail(value, idErr) {
  var reg =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

  let isEmail = reg.test(value);
  if (isEmail) {
    showMessengeErr(idErr, "");
    return true;
  } else {
    showMessengeErr(idErr, "Email không đúng định dạng");
    return false;
  }
}

// filter
// includes
