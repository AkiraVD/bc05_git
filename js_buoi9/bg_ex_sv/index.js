var dssv = [];
const DSSV = "DSSV";

//lưu vào storage
function luuLocalStorage() {
  let jsonDssv = JSON.stringify(dssv);
  localStorage.setItem(DSSV, jsonDssv);
}

//Lấy dữ liệu từ localStorage khi user load
var dataJSON = localStorage.getItem(DSSV);
// console.log("dataJSON: ", dataJSON);
if (dataJSON !== null) {
  var svArr = JSON.parse(dataJSON);

  //Convert data vì dữ liệu không có method
  svArr.forEach(function (item) {
    var sv = new SinhVien(
      item.ma,
      item.ten,
      item.email,
      item.matKhau,
      item.toan,
      item.ly,
      item.hoa
    );
    dssv.push(sv);
    // console.log("dssv: ", dssv);
  });
  renderDssv(dssv);
}

function themSV() {
  var sv = layThongTinTuForm();

  var isValid = true;
  // validate MaSV
  isValid =
    kiemTraRong(sv.ma, "spanMaSV", "Mã SV không được để trống") &&
    kiemTratrung(sv.ma, dssv) &&
    kiemTraSo(sv.ma, "spanMaSV", "Mã sinh viên phải là số");

  //validate tenSV
  isValid =
    isValid &
    kiemTraRong(sv.ten, "spanTenSV", "Vui lòng nhập tên") &
    kiemTraRong(sv.matKhau, "spanMatKhau", "Vui lòng nhập mật khẩu") &
    kiemTraRong(sv.toan, "spanToan", "Vui lòng nhập điểm toán") &
    kiemTraRong(sv.ly, "spanLy", "Vui lòng nhập điểm lý") &
    kiemTraRong(sv.hoa, "spanHoa", "Vui lòng nhập điểm hóa");

  //validate Email
  isValid =
    isValid &
    (kiemTraRong(sv.email, "spanEmailSV", "Vui lòng nhập email") &&
      kiemTraEmail(sv.email, "spanEmailSV"));
  if (isValid) {
    //Add vao array
    dssv.push(sv);

    //lưu vào storage
    luuLocalStorage();

    //render danh sach sinh vien
    renderDssv(dssv);
  }
}

function xoaSV(id) {
  var viTri = timKiemViTri(id, dssv);

  if (viTri !== -1) {
    dssv.splice(viTri, 1);
    //render lại giao diện sau khi xóa
    renderDssv(dssv);
  }

  //lưu vào storage
  luuLocalStorage();
}

function suaSV(id) {
  var viTri = timKiemViTri(id, dssv);
  if (viTri == -1) return;

  var data = dssv[viTri];

  showThongTinLenForm(data);

  //Ngăn cản user edit id sv
  document.getElementById("txtMaSV").disabled = true;
}

function capNhatSV() {
  var svCapNhat = layThongTinTuForm();
  var viTri = timKiemViTri(svCapNhat.ma, dssv);
  if (viTri == -1) return;

  //Cập Nhật
  dssv[viTri] = svCapNhat;

  //lưu vào storage
  luuLocalStorage();

  //render danh sach sinh vien sau khi cap nhat
  renderDssv(dssv);

  //remove disable có trong resetForm
  //reset Form
  resetForm();
}
// CRUD
// creat read update delete
