function SinhVien(maSV, tenSv, email, matKhau, diemToan, diemLy, diemHoa) {
  this.ma = maSV;
  this.ten = tenSv;
  this.email = email;
  this.matKhau = matKhau;
  this.toan = diemToan;
  this.ly = diemLy;
  this.hoa = diemHoa;
  //   this.dtb = ((this.toan + this.ly + this.hoa) / 3).toFixed(2);
  this.tinhDTB = function () {
    return (this.toan * 1 + this.ly * 1 + this.hoa * 1) / 3;
  };
}

function showThongTinLenForm(data) {
  document.querySelector("#txtMaSV").value = data.ma;
  document.querySelector("#txtTenSV").value = data.ten;
  document.querySelector("#txtEmail").value = data.email;
  document.querySelector("#txtPass").value = data.matKhau;
  document.querySelector("#txtDiemToan").value = data.toan;
  document.querySelector("#txtDiemLy").value = data.ly;
  document.querySelector("#txtDiemHoa").value = data.hoa;
}
