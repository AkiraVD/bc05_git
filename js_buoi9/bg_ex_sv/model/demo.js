function cat(catName, catAge) {
  this.name = catName;
  this.age = catAge;
  this.talk = function () {
    console.log("My name is ", this.name);
  };
}

var cat1 = {
  name: "Alice",
  age: 2,
};

var cat2 = new cat("Tom", 2);
